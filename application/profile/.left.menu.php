<?
$aMenuLinks = Array(
    Array(
        "Список покупок",
        "/profile/orders/",
        Array(),
        Array(
            'ICON' => 'fas fa-box',
        ),
        ""
    ),
    Array(
        "Избранные товары",
        "/profile/favorite/",
        Array(),
        Array(
            'ICON' => 'fas fa-heart'
        ),
        ""
    ),
    Array(
        "Настройки профиля",
        "/profile/settings/",
        Array(),
        Array(
            'ICON' => 'fas fa-sliders-h'
        ),
        ""
    ),
    Array(
        "Адреса доставки",
        "/profile/delivery/",
        Array(),
        Array(
            'ICON' => 'fas fa-map-marked-alt'
        ),
        ""
    ),
);
?>