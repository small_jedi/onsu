<?php
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
?>

<?php
$APPLICATION->IncludeComponent(
    "dev:profile",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/profile/",
        "SEF_URL_TEMPLATES" => array(
            "logout" => "logout/",
            "personal" => "",
            "favorite" => "favorite/",
            "settings" => "settings/",
            "delivery" => "delivery/",
            "orders" => "orders/",
            "order" => "orders/#ELEMENT_ID#/",
        )
    ),
    false
);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>