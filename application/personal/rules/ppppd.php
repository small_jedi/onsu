<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("ПОЛИТИКА ЗАЩИТЫ И ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ");
?>
<STYLE type="text/css">

    #page_1 {position:relative; overflow: hidden;margin: 77px 0px 97px 113px;padding: 0px;border: none;width: 681px;}

    .ft0{font: bold 16px 'Times New Roman';line-height: 19px;}
    .ft1{font: 16px 'Times New Roman';line-height: 19px;}
    .ft2{font: bold 16px 'Times New Roman';margin-left: 4px;line-height: 19px;}
    .ft3{font: 16px 'Times New Roman';line-height: 18px;}
    .ft4{font: 16px 'Times New Roman';margin-left: 12px;line-height: 18px;}
    .ft5{font: 16px 'Times New Roman';margin-left: 17px;line-height: 18px;}
    .ft6{font: 16px 'Times New Roman';margin-left: 4px;line-height: 19px;}
    .ft7{font: 16px 'Times New Roman';margin-left: 7px;line-height: 19px;}
    .ft8{font: 16px 'Times New Roman';margin-left: 4px;line-height: 18px;}
    .ft9{font: 16px 'Times New Roman';margin-left: 6px;line-height: 18px;}
    .ft10{font: 16px 'Times New Roman';margin-left: 10px;line-height: 18px;}
    .ft11{font: 16px 'Times New Roman';margin-left: 8px;line-height: 18px;}
    .ft12{font: 16px 'Times New Roman';margin-left: 13px;line-height: 18px;}
    .ft13{font: 16px 'Times New Roman';margin-left: 5px;line-height: 18px;}
    .ft14{font: 16px 'Times New Roman';margin-left: 11px;line-height: 18px;}
    .ft15{font: 16px 'Times New Roman';margin-left: 20px;line-height: 18px;}
    .ft16{font: 16px 'Times New Roman';margin-left: 16px;line-height: 18px;}
    .ft17{font: 16px 'Times New Roman';margin-left: 14px;line-height: 18px;}
    .ft18{font: 16px 'Times New Roman';margin-left: 9px;line-height: 19px;}
    .ft19{font: 16px 'Times New Roman';margin-left: 15px;line-height: 19px;}
    .ft20{font: 16px 'Times New Roman';margin-left: 17px;line-height: 19px;}
    .ft21{font: 16px 'Times New Roman';margin-left: 9px;line-height: 18px;}
    .ft22{font: 16px 'Times New Roman';margin-left: 6px;line-height: 19px;}

    .p0{text-align: left;padding-left: 50px;margin-top: 0px;margin-bottom: 0px;}
    .p1{text-align: left;padding-left: 80px;margin-top: 0px;margin-bottom: 0px;}
    .p2{text-align: left;padding-left: 237px;margin-top: 17px;margin-bottom: 0px;}
    .p3{text-align: justify;padding-right: 57px;margin-top: 0px;margin-bottom: 0px;}
    .p4{text-align: justify;padding-right: 56px;margin-top: 1px;margin-bottom: 0px;}
    .p5{text-align: left;margin-top: 4px;margin-bottom: 0px;}
    .p6{text-align: left;padding-left: 178px;margin-top: 15px;margin-bottom: 0px;}
    .p7{text-align: left;padding-right: 57px;margin-top: 0px;margin-bottom: 0px;}
    .p8{text-align: justify;padding-right: 57px;margin-top: 2px;margin-bottom: 0px;}
    .p9{text-align: justify;padding-right: 57px;margin-top: 1px;margin-bottom: 0px;}
    .p10{text-align: justify;padding-right: 57px;margin-top: 6px;margin-bottom: 0px;}
    .p11{text-align: left;padding-left: 181px;margin-top: 17px;margin-bottom: 0px;}
    .p12{text-align: left;margin-top: 0px;margin-bottom: 0px;}
    .p13{text-align: left;margin-top: 3px;margin-bottom: 0px;}
    .p14{text-align: left;padding-right: 413px;margin-top: 0px;margin-bottom: 0px;}
    .p15{text-align: left;padding-right: 58px;margin-top: 0px;margin-bottom: 0px;}
    .p16{text-align: justify;padding-right: 58px;margin-top: 0px;margin-bottom: 0px;}
    .p17{text-align: left;margin-top: 2px;margin-bottom: 0px;}
    .p18{text-align: left;padding-right: 336px;margin-top: 0px;margin-bottom: 0px;}
    .p19{text-align: left;padding-right: 151px;margin-top: 0px;margin-bottom: 0px;}
    .p20{text-align: left;padding-right: 365px;margin-top: 0px;margin-bottom: 0px;}
    .p21{text-align: left;padding-left: 193px;margin-top: 17px;margin-bottom: 0px;}
    .p22{text-align: justify;padding-right: 58px;margin-top: 1px;margin-bottom: 0px;}
    .p23{text-align: left;padding-right: 57px;margin-top: 2px;margin-bottom: 0px;}
    .p24{text-align: left;margin-top: 1px;margin-bottom: 0px;}
    .p25{text-align: left;padding-left: 101px;margin-top: 18px;margin-bottom: 0px;}
    .p26{text-align: left;padding-right: 57px;margin-top: 1px;margin-bottom: 0px;}
    .p27{text-align: left;padding-right: 479px;margin-top: 0px;margin-bottom: 0px;}
</STYLE>

<DIV id="page_1">

    <P class="p0 ft0">ПОЛИТИКА ЗАЩИТЫ И ОБРАБОТКИ ПЕРСОНАЛЬНЫХ ДАННЫХ</P>
    <P class="p1 ft1">Общества с ограниченной ответственностью «СВЕЖЕЕ К СТОЛУ»</P>
    <P class="p2 ft0"><SPAN class="ft0">1.</SPAN><SPAN class="ft2">Общие положения</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">1.1.</SPAN><SPAN class="ft4">Настоящая Политика в отношении обработки персональных данных (далее – Политика) составлена в соответствии с пунктом 2 статьи 18.1 Федерального закона «О персональных данных» № </SPAN><NOBR>152-ФЗ</NOBR> от 27 июля 2006 г., а также иными нормативно- правовыми актами Российской Федерации в области защиты и обработки персональных данных и действует в отношении всех персональных данных (далее – Данные), которые Организация (далее – Оператор, Общество) может получить от субъекта персональных данных, являющегося стороной по <NOBR>гражданско-правовому</NOBR> договору, а также от субъекта персональных данных, состоящего с Оператором в отношениях, регулируемых трудовым законодательством (далее – Работник).</P>
    <P class="p4 ft3"><SPAN class="ft3">1.2.</SPAN><SPAN class="ft5">Оператор обеспечивает защиту обрабатываемых персональных данных от несанкционированного доступа и разглашения, неправомерного использования или утраты в соответствии с требованиями Федерального закона от 27 июля 2006 г. № </SPAN><NOBR>152-ФЗ</NOBR> «О персональных данных».</P>
    <P class="p5 ft1"><SPAN class="ft1">1.3.</SPAN><SPAN class="ft6">Изменение Политики</SPAN></P>
    <P class="p3 ft1"><SPAN class="ft1">1.3.1.</SPAN><SPAN class="ft7">Оператор имеет право вносить изменения в настоящую Политику. При внесении изменений в заголовке Политики указывается дата последнего обновления редакции. Новая редакция Политики вступает в силу с момента ее размещения на сайте, если иное не предусмотрено новой редакцией Политики.</SPAN></P>
    <P class="p6 ft0"><SPAN class="ft0">2.</SPAN><SPAN class="ft2">Термины и принятые сокращения</SPAN></P>
    <P class="p7 ft3">Персональные данные (ПД) – любая информация, относящаяся к прямо или косвенно определенному или определяемому физическому лицу (субъекту персональных данных). Обработка персональных данных – любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.</P>
    <P class="p8 ft3">Автоматизированная обработка персональных данных – обработка персональных данных с помощью средств вычислительной техники.</P>
    <P class="p3 ft3">Информационная система персональных данных (ИСПД) – совокупность содержащихся в базах данных персональных данных и обеспечивающих их обработку информационных технологий и технических средств.</P>
    <P class="p8 ft3">Персональные данные, сделанные общедоступными субъектом персональных данных, – ПД, доступ неограниченного круга лиц к которым предоставлен субъектом персональных данных либо по его просьбе.</P>
    <P class="p9 ft3">Блокирование персональных данных – временное прекращение обработки персональных данных (за исключением случаев, если обработка необходима для уточнения персональных данных).</P>
    <P class="p8 ft1">Уничтожение персональных данных – действия, в результате которых становится невозможным восстановить содержание персональных данных в информационной системе персональных данных и (или) в результате которых уничтожаются материальные носители персональных данных.</P>
    <P class="p10 ft1">Оператор – организация, самостоятельно или совместно с другими лицами организующая обработку персональных данных, а также определяющая цели обработки персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными. Оператором является Общество с ограниченной ответственностью «СВЕЖЕЕ К</P>

    <P class="p7 ft1">СТОЛУ», ОГРН 1162225080324, расположенное по адресу: 659321, Алтайский край, г.Бийск, ул.Машиностроителей д.24, офис 6.</P>
    <P class="p11 ft0"><SPAN class="ft0">3.</SPAN><SPAN class="ft2">Обработка персональных данных</SPAN></P>
    <P class="p12 ft3"><SPAN class="ft3">3.1.</SPAN><SPAN class="ft8">Получение ПД.</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">3.1.1.</SPAN><SPAN class="ft9">Все ПД следует получать от самого субъекта. Если ПД субъекта можно получить только у третьей стороны, то субъект должен быть уведомлен об этом или от него должно быть получено согласие.</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">3.1.2.</SPAN><SPAN class="ft10">Оператор должен сообщить субъекту о целях, предполагаемых источниках и способах получения ПД, характере подлежащих получению ПД, перечне действий с ПД, сроке, в течение которого действует согласие, и порядке его отзыва, а также о последствиях отказа субъекта дать письменное согласие на их получение.</SPAN></P>
    <P class="p13 ft1"><SPAN class="ft1">3.1.3.</SPAN><SPAN class="ft6">Документы, содержащие ПД, создаются путем:</SPAN></P>
    <P class="p14 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">внесения сведений в учетные формы; 3.2. Обработка ПД.</SPAN></P>
    <P class="p12 ft1">3.2.1. Обработка персональных данных осуществляется:</P>
    <P class="p12 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">с согласия субъекта персональных данных на обработку его персональных данных;</SPAN></P>
    <P class="p15 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft11">в случаях, когда обработка персональных данных необходима для осуществления и выполнения возложенных законодательством РФ функций, полномочий и обязанностей;</SPAN></P>
    <P class="p16 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft5">в случаях, когда осуществляется обработка персональных данных, доступ неограниченного круга лиц к которым предоставлен субъектом персональных данных либо по его просьбе (далее – персональные данные, сделанные общедоступными субъектом персональных данных).</SPAN></P>
    <P class="p17 ft1">3.2.2. Цели обработки персональных данных:</P>
    <P class="p12 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft6">осуществление </SPAN><NOBR>гражданско-правовых</NOBR> отношений.</P>
    <P class="p18 ft3"><SPAN class="ft3">3.2.3.</SPAN><SPAN class="ft8">Категории субъектов персональных данных. Обрабатываются ПД следующих субъектов ПД:</SPAN></P>
    <P class="p12 ft1">– физические лица, состоящие с Обществом в <NOBR>гражданско-правовых</NOBR> отношениях.</P>
    <P class="p12 ft3"><SPAN class="ft3">3.2.4.</SPAN><SPAN class="ft8">ПД, обрабатываемые Оператором:</SPAN></P>
    <P class="p19 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">данные, полученные при осуществлении </SPAN><NOBR>гражданско-правовых</NOBR> отношений. 3.2.5. Обработка персональных данных ведется:</P>
    <P class="p12 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft6">с использованием средств автоматизации;</SPAN></P>
    <P class="p12 ft1"><SPAN class="ft1">3.3.</SPAN><SPAN class="ft6">Хранение ПД.</SPAN></P>
    <P class="p15 ft3"><SPAN class="ft3">3.3.1.</SPAN><SPAN class="ft12">ПД субъектов могут быть получены, проходить дальнейшую обработку и передаваться на хранение как на бумажных носителях, так и в электронном виде.</SPAN></P>
    <P class="p15 ft3"><SPAN class="ft3">3.3.2.</SPAN><SPAN class="ft9">ПД субъектов, обрабатываемые с использованием средств автоматизации в разных целях, хранятся в разных папках.</SPAN></P>
    <P class="p15 ft3"><SPAN class="ft3">3.3.3.</SPAN><SPAN class="ft9">Не допускается хранение и размещение документов, содержащих ПД, в открытых электронных каталогах (файлообменниках) в ИСПД.</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">3.3.4.</SPAN><SPAN class="ft9">Хранение ПД в форме, позволяющей определить субъекта ПД, осуществляется не дольше, чем этого требуют цели их обработки, и они подлежат уничтожению по достижении целей обработки или в случае утраты необходимости в их достижении.</SPAN></P>
    <P class="p13 ft1">3.4. Уничтожение ПД.</P>
    <P class="p15 ft3"><SPAN class="ft3">3.4.1.</SPAN><SPAN class="ft13">ПД на электронных носителях уничтожаются путем стирания или форматирования носителя.</SPAN></P>
    <P class="p12 ft1">3.5. Передача ПД.</P>
    <P class="p12 ft3"><SPAN class="ft3">3.5.1.</SPAN><SPAN class="ft8">Оператор передает ПД третьим лицам в следующих случаях:</SPAN></P>
    <P class="p12 ft1">– субъект выразил свое согласие на такие действия;</P>
    <P class="p15 ft3">– передача предусмотрена российским или иным применимым законодательством в рамках установленной законодательством процедуры.</P>
    <P class="p20 ft1"><SPAN class="ft1">3.5.2.</SPAN><SPAN class="ft6">Перечень лиц, которым передаются ПД. Третьи лица, которым передаются ПД:</SPAN></P>

    <P class="p12 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft6">органы МВД России в случаях, установленных законодательством.</SPAN></P>
    <P class="p21 ft0"><SPAN class="ft0">4.</SPAN><SPAN class="ft2">Защита персональных данных</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">4.1.</SPAN><SPAN class="ft14">В соответствии с требованиями нормативных документов Оператором создана система защиты персональных данных (СЗПД), состоящая из подсистем правовой, организационной и технической защиты.</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">4.2.</SPAN><SPAN class="ft15">Подсистема правовой защиты представляет собой комплекс правовых, </SPAN><NOBR>организационно-распорядительных</NOBR> и нормативных документов, обеспечивающих создание, функционирование и совершенствование СЗПД.</P>
    <P class="p22 ft3"><SPAN class="ft3">4.3.</SPAN><SPAN class="ft14">Подсистема организационной защиты включает в себя организацию структуры управления СЗПД, разрешительной системы, защиты информации при работе с сотрудниками, партнерами и сторонними лицами.</SPAN></P>
    <P class="p23 ft3"><SPAN class="ft3">4.4.</SPAN><SPAN class="ft16">Подсистема технической защиты включает в себя комплекс технических, программных, </SPAN><NOBR>программно-аппаратных</NOBR> средств, обеспечивающих защиту ПД.</P>
    <P class="p17 ft1"><SPAN class="ft1">4.5.</SPAN><SPAN class="ft6">Основными мерами защиты ПД, используемыми Оператором, являются:</SPAN></P>
    <P class="p16 ft3"><SPAN class="ft3">4.5.1.</SPAN><SPAN class="ft17">Назначение лица, ответственного за обработку ПД, которое осуществляет организацию обработки ПД, обучение и инструктаж, внутренний контроль за соблюдением учреждением и его работниками требований к защите ПД.</SPAN></P>
    <P class="p7 ft1"><SPAN class="ft1">4.5.2.</SPAN><SPAN class="ft18">Определение актуальных угроз безопасности ПД при их обработке в ИСПД и разработка мер и мероприятий по защите ПД.</SPAN></P>
    <P class="p12 ft3"><SPAN class="ft3">4.5.3.</SPAN><SPAN class="ft8">Разработка политики в отношении обработки персональных данных.</SPAN></P>
    <P class="p7 ft3"><SPAN class="ft3">4.5.4.</SPAN><SPAN class="ft13">Установление правил доступа к ПД, обрабатываемым в ИСПД, а также обеспечение регистрации и учета всех действий, совершаемых с ПД в ИСПД.</SPAN></P>
    <P class="p7 ft1"><SPAN class="ft1">4.5.5.</SPAN><SPAN class="ft7">Установление индивидуальных паролей доступа сотрудников в информационную систему в соответствии с их производственными обязанностями.</SPAN></P>
    <P class="p15 ft3"><SPAN class="ft3">4.5.6.</SPAN><SPAN class="ft9">Применение прошедших в установленном порядке процедуру оценки соответствия средств защиты информации.</SPAN></P>
    <P class="p7 ft3"><SPAN class="ft3">4.5.7.</SPAN><SPAN class="ft5">Сертифицированное антивирусное программное обеспечение с регулярно обновляемыми базами.</SPAN></P>
    <P class="p7 ft1"><SPAN class="ft1">4.5.8.</SPAN><SPAN class="ft19">Соблюдение условий, обеспечивающих сохранность ПД и исключающих несанкционированный к ним доступ.</SPAN></P>
    <P class="p15 ft3"><SPAN class="ft3">4.5.9.</SPAN><SPAN class="ft11">Обнаружение фактов несанкционированного доступа к персональным данным и принятие мер.</SPAN></P>
    <P class="p7 ft1"><SPAN class="ft1">4.5.10.</SPAN><SPAN class="ft20">Восстановление ПД, модифицированных или уничтоженных вследствие несанкционированного доступа к ним.</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">4.5.11.</SPAN><SPAN class="ft11">Обучение работников Оператора, непосредственно осуществляющих обработку персональных данных, положениям законодательства РФ о персональных данных, в том числе требованиям к защите персональных данных, документам, определяющим политику Оператора в отношении обработки персональных данных, локальным актам по вопросам обработки персональных данных.</SPAN></P>
    <P class="p24 ft1"><SPAN class="ft1">4.5.12.</SPAN><SPAN class="ft6">Осуществление внутреннего контроля и аудита.</SPAN></P>
    <P class="p25 ft0"><SPAN class="ft0">5.</SPAN><SPAN class="ft2">Основные права субъекта ПД и обязанности Оператора</SPAN></P>
    <P class="p12 ft3"><SPAN class="ft3">5.1.</SPAN><SPAN class="ft8">Основные права субъекта ПД.</SPAN></P>
    <P class="p12 ft1">Субъект имеет право на доступ к его персональным данным и следующим сведениям:</P>
    <P class="p12 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">подтверждение факта обработки ПД Оператором;</SPAN></P>
    <P class="p12 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">правовые основания и цели обработки ПД;</SPAN></P>
    <P class="p12 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft6">цели и применяемые Оператором способы обработки ПД;</SPAN></P>
    <P class="p3 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft21">наименование и место нахождения Оператора, сведения о лицах (за исключением работников Оператора), которые имеют доступ к ПД или которым могут быть раскрыты ПД на основании договора с Оператором или на основании федерального закона;</SPAN></P>
    <P class="p12 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft6">сроки обработки персональных данных, в том числе сроки их хранения;</SPAN></P>

    <P class="p15 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">порядок осуществления субъектом ПД прав, предусмотренных настоящим Федеральным законом;</SPAN></P>
    <P class="p26 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft22">наименование или фамилия, имя, отчество и адрес лица, осуществляющего обработку ПД по поручению Оператора, если обработка поручена или будет поручена такому лицу;</SPAN></P>
    <P class="p12 ft3"><SPAN class="ft3">–</SPAN><SPAN class="ft8">обращение к Оператору и направление ему запросов;</SPAN></P>
    <P class="p12 ft1"><SPAN class="ft1">–</SPAN><SPAN class="ft6">обжалование действий или бездействия Оператора.</SPAN></P>
    <P class="p27 ft3"><SPAN class="ft3">5.2.</SPAN><SPAN class="ft8">Обязанности Оператора. Оператор обязан:</SPAN></P>
    <P class="p12 ft1">– при сборе ПД предоставить информацию об обработке ПД;</P>
    <P class="p12 ft3">– в случаях если ПД были получены не от субъекта ПД, уведомить субъекта;</P>
    <P class="p12 ft1">– при отказе в предоставлении ПД субъекту разъясняются последствия такого отказа;</P>
    <P class="p3 ft3">– опубликовать или иным образом обеспечить неограниченный доступ к документу, определяющему его политику в отношении обработки ПД, к сведениям о реализуемых требованиях к защите ПД;</P>
    <P class="p3 ft3">– принимать необходимые правовые, организационные и технические меры или обеспечивать их принятие для защиты ПД от неправомерного или случайного доступа к ним, уничтожения, изменения, блокирования, копирования, предоставления, распространения ПД, а также от иных неправомерных действий в отношении ПД;</P>
    <P class="p23 ft1">– давать ответы на запросы и обращения субъектов ПД, их представителей и уполномоченного органа по защите прав субъектов ПД.</P>
</DIV>

<? require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php" ?>
