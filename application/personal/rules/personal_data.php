<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Согласие на обработку персональных данных"); ?>
<main class="container about-page">
    <div class="row">
        <div class="col-xs-12">
            <h1><? $APPLICATION->ShowTitle() ?></h1>
            <p>Я даю согласие ООО «СВЕЖЕЕ К СТОЛУ», ОГРН 1162225080324, которое находится по адресу:
                659321, Алтайский край, г. Бийск, ул. Машиностроителей д.24 офис 6, в соответствии со статьей 9
                Федерального закона от 27 июля 2006 г. № 152-ФЗ «О персональных данных» на обработку моих
                персональных данных, а именно на совершение действий, предусмотренных пунктом 3 статьи 3
                Федерального закона от 27 июля 2006 г. № 152-ФЗ «О персональных данных».&nbsp; &nbsp;
            </p>
            <p>Цель обработки персональных данных – пользоваться сайтом ООО «СВЕЖЕЕ К СТОЛУ», расположенном в
                сети интернет по адресу
                <a href="http://www.provianta.ru/">http://www.provianta.ru</a>,
                задавать вопросы Оператору по поводу заказа и производства продуктов питания, используемого
                сырья, и т.д.,&nbsp; оставлять свои отзывы и предложения по работе компании, предоставлять сведения и
                предложения по работе сайта.
            </p>
            <p>Настоящее согласие даю на период до истечения сроков хранения соответствующей информации или
                документов, содержащих указанную информацию, определяемых в соответствии с законом РФ.
            </p>
            <p>Настоящим согласием подтверждаю, что ознакомлен(а) с<br>
                <a href="/personal/rules/ps.php">Пользовательским соглашением</a>,<br>
                <a href="/personal/rules/rpsppd.php">Положением о порядке хранения и защиты персональных данных пользователей</a>,<br>
                <a href="/personal/rules/ppppd.php">Политикой защиты и обработке персональных данных</a>,<br>
                представленных в свободном доступе на сайте
                <a href="http://www.provianta.ru/">http://www.provianta.ru</a>.&nbsp;
            </p>
        </div>
    </div>
</main>
<? require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php" ?>