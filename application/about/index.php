<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");
?>
<style>
    .bcrumbs-container {
        margin-bottom: 0;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-3 hidden-xs" >
            <img src="1.jpg" width="100%"/>
        </div>
        <div class="col-md-9 col-xs-12">
            <h1><?$APPLICATION->ShowTitle()?></h1>
            <p>Наша компания реализует <a href="/catalog/zamorozhennye_polufabrikaty_/">замороженные полуфабрикаты</a>
                (пельмени, вареники, манты, хинкали, котлеты, тефтели), <a href="/catalog/kolbasa_i_sosiski/">колбасы</a>
                (краковская, сосиски молочные, колбаски гриль), <a href="/catalog/myasnye_delikatesy/">мясные деликатесы</a>
                (карбонат, грудинка, балык, рулет мясной, холодец), <a href="/catalog/bliny_shtrudeli/">блины</a>
                (с мясной и сладкой начинкой), <a href="/catalog/samsa_chebureki/">пироги</a> (самса, чебуреки, штрудели). </p>
            <p>Мы напрямую сотрудничаем с алтайскими производителями продуктов питания. Мы выбираем для вас производителей,
                которые заботятся о натуральности и высоком качестве своей продукции. </p>
            <a href="/about/info/" >Подробнее...</a><br>
            <br>
            <p>Информация о наших <a href="/about/provider/" >Поставщиках</a></p><br>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
