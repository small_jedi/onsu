<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поставщики");
?>
<main class="container provider-page">
    <div class="row">
        <div class="col-xs-12">
            <h1>Наши поставщики</h1>
            <p>Мы напрямую сотрудничаем с алтайскими производителями продуктов питания. Наши поставщики заботятся о натуральности
             и высоком качестве своей продукции.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 provider-img-wrap">
            <img src="ss.jpg">
        </div>
        <div class="col-md-10">
            <p><b>ООО "СВЕЖЕЕ К СТОЛУ" </b>(659321, г.Бийск, ул.Машиностроителей д.24, оф.6) - производство полуфабрикатов,
            колбас, мясных деликатесов.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 provider-img-wrap">
            <img src="ab.jpg">
        </div>
        <div class="col-md-10">
            <p><b>ООО "Алтайская буренка" </b>(659405, Алтайский край, Зональный р-н, с.Буланиха, ул.Школьная 1Б) -
            производство молочных продуктов.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 provider-img-wrap">
            <img src="pr.jpg">
        </div>
        <div class="col-md-10">
            <p><b>ООО "ПряниковЪ" </b>(656922,&nbsp;Алтайский край,&nbsp;город Барнаул,&nbsp;Трактовая улица, дом 7/2) -
            производство муки.</p>
        </div>
    </div>
</main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>