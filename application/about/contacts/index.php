<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><main class="container about-page">
<div class="row">
	<div class="col-xs-12">
		<h1><?$APPLICATION->ShowTitle()?></h1>
		<div>
			<p>
				 ООО «СВЕЖЕЕ К СТОЛУ»<br>
			</p>
			<p>
				 ОГРН 1162225080324<br>
			</p>
			<p>
				 Юридический адрес:&nbsp;
			</p>
			<p>
				 659321, Россия, Алтайский край, г.Бийск, ул.Машиностроителей д.24, офис 6<br>
			</p>
			<p>
				 E-mail: <a href="mailto:shop@provianta.ru">shop@provianta.ru</a>
			</p>
			<p>
				 тел. 8-923-792-72-22
			</p>
		</div>
	</div>
</div>
 </main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>