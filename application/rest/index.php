<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule(DEV_MODULE_ID)) throw new Exception('Модуль good.begin не подключен');

$rest = new \Good\Begin\Rest\RestApi();
$rest->handle();