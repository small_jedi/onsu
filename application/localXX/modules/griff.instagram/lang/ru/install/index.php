<?php
/**
 * Created by PhpStorm.
 * User: Gredasow Iwan (Griff19)
 * Date: 24.06.2019
 * Time: 2:15
 */

$MESS["GRIFF_INSTA_NAME"] = "Товары в Instagram";
$MESS["GRIFF_INSTA_DESCRIPTION"] = "Выгружает товары с картинками в Instagram";
$MESS["GRIFF_INSTA_PARTNER_NAME"] = "Гредасов Иван";
$MESS["GRIFF_INSTA_PARTNER_URI"] = "https://vk.com/gredasov";