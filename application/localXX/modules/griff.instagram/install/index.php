<?php
/**
 * Created by PhpStorm.
 * User: Gredasow Iwan (Griff19)
 * Date: 24.06.2019
 * Time: 2:11
 */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class griff_instagram extends CModule{
    
    public function __construct(){
        if (file_exists(__DIR__."/version.php")){
            $arModuleVersion = [];
            
            include_once(__DIR__."/version.php");
            
            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("GRIFF_INSTA_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("GRIFF_INSTA_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage("GRIFF_INSTA_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("GRIFF_INSTA_PARTNER_URI");
        }
        
        return false;
    }
    
}
