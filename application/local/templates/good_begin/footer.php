<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


</div>


<footer class="footer">
    <div class="container">
        <div class="footer-row">
            <div class="footer-col">
                <div class="footer-title">
                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_TEMPLATE_PATH . '/include/footer-title.php',
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    );
                    ?>
                </div>
                <div class="footer-descr">
                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => SITE_TEMPLATE_PATH . '/include/footer-descr.php',
                            "EDIT_TEMPLATE" => ""
                        ),
                        false
                    );
                    ?>
                </div>
                <?php $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_TEMPLATE_PATH . '/include/footer-mail.php',
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );
                $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_TEMPLATE_PATH . '/include/footer-phone.php',
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );
                $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer_catalog_menu",
                    array(
                        "COMPONENT_TEMPLATE" => "footer_catalog_menu",
                        "ROOT_MENU_TYPE" => "top",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
            </div>
            <div class="footer-col">
                <?php $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer_catalog_menu",
                    array(
                        "COMPONENT_TEMPLATE" => "footer_catalog_menu",
                        "ROOT_MENU_TYPE" => "catalog",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                ); ?>
            </div>
            <div class="footer-col">
                <ul class="footer-menu">
                    <li class="footer-menu-item"><a class="footer-menu-link site-link" href="#">Согласие на обработку персональных данных</a></li>
                </ul>
            </div>
            <div class="footer-col">

                <?php
                $APPLICATION->IncludeComponent("bitrix:news.list", "social", Array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "IBLOCK_TYPE" => "content",    // Тип информационного блока (используется только для проверки)
                    "IBLOCK_ID" => "87",    // Код информационного блока
                    "NEWS_COUNT" => "20",    // Количество новостей на странице
                    "SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
                    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
                    "SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
                    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
                    "FILTER_NAME" => "",    // Фильтр
                    "FIELD_CODE" => array(    // Поля
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(    // Свойства
                        0 => "",
                        1 => "",
                    ),
                    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
                    "DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                    "AJAX_MODE" => "N",    // Включить режим AJAX
                    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                    "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                    "CACHE_TYPE" => "A",    // Тип кеширования
                    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
                    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
                    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                    "SET_BROWSER_TITLE" => "Y",    // Устанавливать заголовок окна браузера
                    "SET_META_KEYWORDS" => "Y",    // Устанавливать ключевые слова страницы
                    "SET_META_DESCRIPTION" => "Y",    // Устанавливать описание страницы
                    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
                    "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
                    "PARENT_SECTION" => "",    // ID раздела
                    "PARENT_SECTION_CODE" => "",    // Код раздела
                    "INCLUDE_SUBSECTIONS" => "N",    // Показывать элементы подразделов раздела
                    "STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
                    "DISPLAY_DATE" => "Y",    // Выводить дату элемента
                    "DISPLAY_NAME" => "Y",    // Выводить название элемента
                    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
                    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
                    "PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
                    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
                    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
                    "PAGER_TITLE" => "Новости",    // Название категорий
                    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
                    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
                    "PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
                    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
                    "SET_STATUS_404" => "N",    // Устанавливать статус 404
                    "SHOW_404" => "N",    // Показ специальной страницы
                    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
                ),
                    false
                );
                ?>
                <div class="footer-subscribe">
                    <form class="footer-subscribe-form"><input class="footer-subscribe-form-input" placeholder="Подпишись на обновления">
                        <button class="footer-subscribe-form-submit">
                            <div>Подписаться</div>
                            <i class="fas fa-bell"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</footer><!-- auth modal--><!--#authModal.site-modal.modal.fade(tabindex='-1' role='dialog' aria-labelledby='authModalLabel' aria-hidden='true')--><!--    .modal-dialog(role='document')--><!--        .modal-content--><!--            .modal-header--><!--                h5#authModalLabel.modal-title Modal title--><!--                button.close(type='button' data-dismiss='modal' aria-label='Close')--><!--                    span(aria-hidden='true') ×--><!--            .modal-body--><!--                form.site-form--><!--                    label.site-form-label--><!--                        .site-form-label__caption Логин--><!--                        input.site-form-input--><!--                    label.site-form-label--><!--                        .site-form-label__caption Пароль--><!--                        input.site-form-input--><!--                    button.site-form-submit Отправить--><!--            .modal-footer-->
<div class="site-modal modal fade" id="authModal" tabindex="-1" role="dialog" aria-labelledby="authModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="authModalLabel">Войти на сайт</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <?php $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form",
                    "modal",
                    array(
                        "REGISTER_URL" => "",
                        "FORGOT_PASSWORD_URL" => "/profile/?forgot_password=yes",
                        "PROFILE_URL" => "/profile/",
                        "SHOW_ERRORS" => "Y"
                    )
                ); ?>
                <? /*<form class="site-form">


                    <div class="site-form-label">
                        <label class="site-form-label__caption">Логин</label>
                        <input class="site-form-input"></div>

                    <div class="site-form-label">
                        <label class="site-form-label__caption">Пароль</label>
                        <input class="site-form-input">
                    </div>

                    <a class="site-link" href="#">Регистрация</a>
                    <button class="site-form-submit">Войти на сайт</button>
                </form>*/ ?>
            </div>
        </div>
    </div>
</div>

<script src="<?= ASSETS_DIR; ?>/bundle.js"></script>
</body>
</html>