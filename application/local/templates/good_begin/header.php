<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php use Bitrix\Main\Page\Asset; ?>
    <!doctype html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">


        <script src="https://kit.fontawesome.com/75e4faf288.js" crossorigin="anonymous"></script>

        <?php Asset::getInstance()->addCss('/local/assets/css/main.css'); ?>
        <title><?php $APPLICATION->ShowTitle(); ?></title>
        <?php $APPLICATION->ShowHead(); ?>
    </head>
<?php $APPLICATION->ShowPanel(); ?>
<body>
<div class="container">
    <header class="header-mobile">
        <div class="header-mobile-menu-container">
            <div class="header-mobile-menu-switch header-mobile-icon site-link"><i class="fas fa-bars"></i></div>
            <div class="header-mobile-menu active">
                <div class="header-mobile-menu-close site-link">
                    <div class="header-mobile-menu-close__icon"><i class="far fa-times-circle"></i></div>
                </div>
                <div class="header-mobile-menu-list"><a class="active header-mobile-menu-list-item" href="#">Главная</a><a class="header-mobile-menu-list-item" href="#">Каталог</a><a class="header-mobile-menu-list-item" href="#">О компании</a><a class="header-mobile-menu-list-item" href="#">Акции</a><a class="header-mobile-menu-list-item" href="#">Доставка и оплата</a></div>
                <div class="header-mobile-menu-list"><a class="header-mobile-menu-list-item" href="#">Замороженные полуфабрикаты</a><a class="header-mobile-menu-list-item" href="#">Гриль и шашлык</a><a class="header-mobile-menu-list-item" href="#">Мука</a><a class="header-mobile-menu-list-item" href="#">Молочная продукция</a><a class="header-mobile-menu-list-item" href="#">Мясные деликатесы</a><a class="header-mobile-menu-list-item" href="#">Мясо и субпродукты</a><a class="header-mobile-menu-list-item" href="#">Колбаса и сосиски</a></div>
            </div>
        </div>
        <a class="header-mobile-logo" href="/"><img src="<?= ASSETS_DIR; ?>/images/logo.jpg"></a>
        <div class="header-mobile-phone header-mobile-icon site-link"><i class="fas fa-phone"></i></div>
        <div class="header-mobile-search header-mobile-icon site-link"><i class="fas fa-search"></i></div>
        <div class="header-mobile-cart header-mobile-icon site-link"><i class="fas fa-shopping-cart"></i></div>
    </header>
    <header class="header">
        <div class="header-top-line">
            <?php
            $APPLICATION->IncludeComponent('dev:city.header', '.default', array());
            
            $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top_menu",
                array(
                    "COMPONENT_TEMPLATE" => "top_menu",
                    "ROOT_MENU_TYPE" => "top",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );
            ?>

        </div>
        <div class="header-line">

            <a class="logotype-wrap" href="/"><img class="logotype" src="<?= ASSETS_DIR; ?>/images/logo.jpg"></a>

            <?php $APPLICATION->IncludeComponent(
                "bitrix:search.form",
                "search_form",
                array(
                    "COMPONENT_TEMPLATE" => "search_form",
                    "PAGE" => "#SITE_DIR#search/index.php",
                    "USE_SUGGEST" => "N"
                ),
                false
            ); ?>


            <?php global $USER; ?>
            <?php if (!$USER->IsAuthorized()): ?>
                <div class="header-icon" data-toggle="modal" data-target="#authModal"><i class="header-icon-icon fas fa-user-alt"></i>
                    <div class="header-icon-title">Войти</div>
                </div>
                <div class="header-icon" data-toggle="modal" data-target="#authModal"><i class="header-icon-icon fas fa-box"></i>
                    <div class="header-icon-title">Заказы</div>
                </div>
            <?php else: ?>
                <a href="/profile/" class="header-icon"><i class="header-icon-icon fas fa-user-alt"></i>
                    <div class="header-icon-title">Профиль</div>
                </a>
                <a href="/profile/orders/" class="header-icon"><i class="header-icon-icon fas fa-box"></i>
                    <div class="header-icon-title">Заказы</div>
                </a>
            <?php endif; ?>

            <a href="/favorite/" class="header-icon">
                <?php $count = \Good\Begin\Classes\Services\FavoriteService::count(); ?>
                <div class="header-icon-counter js-favorite-counter <?= $count > 0 ? 'is-active' : '' ?>"><?= $count ?></div>
                <i class="header-icon-icon far fa-heart"></i>
                <div class="header-icon-title">Избранное</div>
            </a>
            <a href="/order/cart/" class="header-icon">
                <?php $countBasket = \Good\Begin\Classes\Services\BasketService::count(); ?>
                <div class="header-icon-counter js-basket-counter <?= $countBasket > 0 ? 'is-active' : '' ?>"><?= $countBasket ?></div>
                <i class="fas fa-shopping-basket"></i>
                <div>Корзина</div>
            </a>
        </div>
        <div class="header-line">
            <?php $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "header_catalog_menu",
                array(
                    "COMPONENT_TEMPLATE" => "header_catalog_menu",
                    "ROOT_MENU_TYPE" => "catalog",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>
        </div>
    </header>

<?php $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "breads",
    array(
        "COMPONENT_TEMPLATE" => "breads",
        "START_FROM" => "0",
        "PATH" => "",
        "SITE_ID" => "s1"
    ),
    false
); ?>