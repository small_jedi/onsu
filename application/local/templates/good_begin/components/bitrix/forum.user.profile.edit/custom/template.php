<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?

use Bitrix\Main\Localization\Loc;

if (!$this->__component->__parent || empty($this->__component->__parent->__name)):
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/forum/templates/.default/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/forum/templates/.default/themes/blue/style.css');
    $GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/forum/templates/.default/styles/additional.css');
endif;
$path = str_replace(array("\\", "//"), "/", dirname(__FILE__) . "/interface.php");
include_once($path);
// *****************************************************************************************
if (!empty($arResult["ERROR_MESSAGE"])):
    ?>
    <div class="forum-note-box forum-note-error">
        <div class="forum-note-box-text"><?= ShowError($arResult["ERROR_MESSAGE"], "forum-note-error"); ?></div>
    </div>
<?php endif;

if (!empty($arResult["OK_MESSAGE"])): ?>
    <div class="forum-note-box forum-note-success">
        <div class="forum-note-box-text"><?= ShowNote($arResult["OK_MESSAGE"], "forum-note-success") ?></div>
    </div>
<?php endif; ?>


<form method="post" name="form1" action="<?= POST_FORM_ACTION_URI ?>" enctype="multipart/form-data" class="forum-form site-form edit-form">
    <input type="hidden" name="PAGE_NAME" value="profile"/>
    <input type="hidden" name="Update" value="Y"/>
    <input type="hidden" name="UID" value="<?= $arParams["UID"] ?>"/>
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="ACTION" value="EDIT"/>

    <div class="site-form-label">
        <label for="NEW_PASSWORD" class="site-form-label__caption"><?= GetMessage("F_NEW_PASSWORD") ?></label>
        <input class="site-form-input" type="password" id="NEW_PASSWORD" name="NEW_PASSWORD" size="20" maxlength="50" value="<?= $arResult["NEW_PASSWORD"] ?>" autocomplete="off"/>
    </div>

    <div class="site-form-label">
        <label for="NEW_PASSWORD_CONFIRM" class="site-form-label__caption"><?= GetMessage("F_PASSWORD_CONFIRM") ?></label>
        <input class="site-form-input" type="password" id="NEW_PASSWORD_CONFIRM" name="NEW_PASSWORD_CONFIRM" size="20" maxlength="50" value="<?= $arResult["NEW_PASSWORD_CONFIRM"] ?>" autocomplete="off"/>
    </div>


    <button class="site-form-submit"><?= Loc::getMessage('F_SAVE') ?></button>
</form>