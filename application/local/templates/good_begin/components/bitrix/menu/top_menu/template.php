<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php if (!empty($arResult)): ?>
    <ul class="top-menu">
        <?php foreach ($arResult as $arItem): ?>
            <?php if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
            <?php if ($arItem["SELECTED"]): ?>
                <li class="top-menu-item active"><a class="top-menu-item-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
            <?php else: ?>
                <li class="top-menu-item"><a class="top-menu-item-link" href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
            <?php endif ?>


        <?php endforeach ?>
    </ul>
<?php endif ?>
