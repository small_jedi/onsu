<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php if (!empty($arResult)): ?>
    <div class="profile-menu">

        <?php foreach ($arResult as $arItem): ?>
            <?php if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>


            <?php if ($arItem["SELECTED"]): ?>
                <a class="profile-menu-item active" href="<?= $arItem["LINK"] ?>"><i class="profile-menu-item__icon <?= $arItem['PARAMS']['ICON'] ?>"></i>
                    <div class="profile-menu-item__title"><?= $arItem["TEXT"] ?></div>
                </a>
            <?php else: ?>
                <a class="profile-menu-item" href="<?= $arItem["LINK"] ?>"><i class="profile-menu-item__icon <?= $arItem['PARAMS']['ICON'] ?>"></i>
                    <div class="profile-menu-item__title"><?= $arItem["TEXT"] ?></div>
                </a>
            <?php endif ?>

        <?php endforeach ?>

    </div>
<?php endif ?>

<?php
/*<div class="profile-menu">
    <a class="profile-menu-item" href="/"><i class="profile-menu-item__icon fas fa-box"></i>
        <div class="profile-menu-item__title">Список покупок</div>
    </a>

    <a class="profile-menu-item" href="/"><i class="profile-menu-item__icon fas fa-heart"></i>
        <div class="profile-menu-item__title">Избранные товары</div>
    </a>

    <a class="profile-menu-item" href="/"><i class="profile-menu-item__icon fas fa-sliders-h"></i>
        <div class="profile-menu-item__title">Настройки профиля</div>
    </a>

    <a class="profile-menu-item" href="/"><i class="profile-menu-item__icon fas fa-map-marked-alt"></i>
        <div class="profile-menu-item__title">Адреса доставки</div>
    </a>
</div>*/
?>
