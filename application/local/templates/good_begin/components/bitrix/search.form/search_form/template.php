<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>
<div class="search-form-wrap">
    <form class="search-form" action="<?= $arResult["FORM_ACTION"] ?>">
        <input class="search-form-input" name="q" placeholder="<?= \Bitrix\Main\Localization\Loc::getMessage('INPUT_PLACEHOLDER'); ?>">
        <button class="search-form-button"><i class="fas fa-search"></i></button>
    </form>
</div>