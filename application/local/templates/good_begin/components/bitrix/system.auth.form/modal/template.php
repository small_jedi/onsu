<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CJSCore::Init();
?>


<?php if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) ShowMessage($arResult['ERROR_MESSAGE']); ?>

<?php if ($arResult["FORM_TYPE"] == "login"): ?>

    <form name="system_auth_form<?= $arResult["RND"] ?>" class="js-auth-form site-form" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
        <?php if ($arResult["BACKURL"] <> ''): ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
        <?php endif ?>
        <?php foreach ($arResult["POST"] as $key => $value): ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
        <?php endforeach ?>
        <input type="hidden" name="AUTH_FORM" value="Y"/>
        <input type="hidden" name="TYPE" value="AUTH"/>


        <div class="site-form-label">
            <label class="site-form-label__caption" for="site-form-USER_LOGIN"><?= GetMessage("AUTH_LOGIN") ?></label>
            <input required class="site-form-input" id="site-form-USER_LOGIN" type="text"  name="USER_LOGIN">
        </div>

        <script>
            BX.ready(function () {
                var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                if (loginCookie) {
                    var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                    var loginInput = form.elements["USER_LOGIN"];
                    loginInput.value = loginCookie;
                }
            });
        </script>

        <div class="site-form-label">
            <label class="site-form-label__caption" for="site-form-USER_PASSWORD"><?= GetMessage("AUTH_PASSWORD") ?></label>
            <input required class="site-form-input" maxlength="255"  autocomplete="off" id="site-form-USER_PASSWORD" type="password" name="USER_PASSWORD">
        </div>


        <?php if ($arResult["SECURE_AUTH"]): ?>
            <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>" title="<?php echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
            <noscript>
				<span class="bx-auth-secure" title="<?php echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
            </noscript>
            <script type="text/javascript">
                document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
            </script>
        <?php endif ?>

        <div class="d-flex align-center justify-content-between">
            <?php if ($arResult["STORE_PASSWORD"] == "Y"): ?>
                <div>
                    <input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y"/>
                    <label for="USER_REMEMBER_frm" title="<?= GetMessage("AUTH_REMEMBER_ME") ?>"><?php echo GetMessage("AUTH_REMEMBER_SHORT") ?></label>
                </div>
            <?php endif ?>

            <noindex><a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>" rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a></noindex>
        </div>


        <?php if ($arResult["CAPTCHA_CODE"]): ?>
            <?php echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
            <input type="hidden" name="captcha_sid" value="<?php echo $arResult["CAPTCHA_CODE"] ?>"/>
            <img src="/bitrix/tools/captcha.php?captcha_sid=<?php echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
            <input type="text" name="captcha_word" maxlength="50" value=""/>
        <?php endif ?>

        <div class="modal-footer">
            <input type="hidden" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/>
            <button type="submit" class="site-form-submit"><?= GetMessage("AUTH_LOGIN_BUTTON") ?></button>
        </div>


        <?php /* if ($arResult["NEW_USER_REGISTRATION"] == "Y"): ?>
                <noindex><a href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a></noindex>
                <br/>
            <?php endif */ ?>


        <?php /* if ($arResult["AUTH_SERVICES"]): ?>
            <div class="bx-auth-lbl"><?= GetMessage("socserv_as_user_form") ?></div>
            <?php $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "icons",
                array(
                    "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                    "SUFFIX" => "form",
                ),
                $component,
                array("HIDE_ICONS" => "Y")
            ); ?>
        <?php endif */ ?>
    </form>

    <?php if ($arResult["AUTH_SERVICES"]): ?>
        <?php $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
            array(
                "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                "AUTH_URL" => $arResult["AUTH_URL"],
                "POST" => $arResult["POST"],
                "POPUP" => "Y",
                "SUFFIX" => "form",
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        ); ?>
    <?php endif ?>

<?php elseif ($arResult["FORM_TYPE"] == "otp"): ?>
    <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
        <?php if ($arResult["BACKURL"] <> ''): ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
        <?php endif ?>
        <input type="hidden" name="AUTH_FORM" value="Y"/>
        <input type="hidden" name="TYPE" value="OTP"/>
        <?php echo GetMessage("auth_form_comp_otp") ?><br/>
        <input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off"/>
        <?php if ($arResult["CAPTCHA_CODE"]): ?>
            <td colspan="2">
                <?php echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
                <input type="hidden" name="captcha_sid" value="<?php echo $arResult["CAPTCHA_CODE"] ?>"/>
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?php echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
                <input type="text" name="captcha_word" maxlength="50" value=""/></td>
        <?php endif ?>
        <?php if ($arResult["REMEMBER_OTP"] == "Y"): ?>
            <td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y"/></td>
            <td width="100%"><label for="OTP_REMEMBER_frm" title="<?php echo GetMessage("auth_form_comp_otp_remember_title") ?>"><?php echo GetMessage("auth_form_comp_otp_remember") ?></label></td>
        <?php endif ?>
        <td colspan="2"><input type="submit" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/></td>
        <noindex><a href="<?= $arResult["AUTH_LOGIN_URL"] ?>" rel="nofollow"><?php echo GetMessage("auth_form_comp_auth") ?></a></noindex>
    </form>

<?php else: ?>
    <form action="<?= $arResult["AUTH_URL"] ?>">
        <?= $arResult["USER_NAME"] ?><br/>
        [<?= $arResult["USER_LOGIN"] ?>]<br/>
        <a href="<?= $arResult["PROFILE_URL"] ?>" title="<?= GetMessage("AUTH_PROFILE") ?>"><?= GetMessage("AUTH_PROFILE") ?></a><br/>
        <?php foreach ($arResult["GET"] as $key => $value): ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
        <?php endforeach ?>
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="logout" value="yes"/>
        <input type="submit" name="logout_butt" value="<?= GetMessage("AUTH_LOGOUT_BUTTON") ?>"/>
    </form>
<?php endif ?>
