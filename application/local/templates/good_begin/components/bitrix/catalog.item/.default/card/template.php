<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Good\Begin\Classes\Services\BasketService;
use Good\Begin\Classes\Services\FavoriteService;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<?php //\Bitrix\Main\Diag\Debug::dump($price); ?>

<div class="catalog-item-wrap">
    <div class="catalog-item">
        <div class="catalog-action">
            <div class="add-to-wishlist-list-react catalog-action-item favorite" data-product-id="<?= $item['ID']; ?>" data-is-exist="<?= FavoriteService::isExist(intval($item['ID'])) ?>"></div>

            <div class="catalog-action-item compare">
                <div class="fas fa-balance-scale-right"></div>
            </div>
        </div>
        <a class="catalog-item-link" href="<?= $item['DETAIL_PAGE_URL']; ?>">
            <img class="catalog-item-image" src="<?= $item['DETAIL_PICTURE']['SAFE_SRC']; ?>">
        </a>
        <a class="catalog-item-link site-link" href="<?= $item['DETAIL_PAGE_URL']; ?>">
            <div class="catalog-item-name"><?= $item['NAME']; ?></div>
        </a>
        <div class="catalog-item-price-group">
            <div class="catalog-item-price"><?= $price['PRINT_PRICE']; ?></div>
            <? /*
            <div class="catalog-item-price-discount">1000 ₽</div>
            <div class="catalog-item-price-old">1500 ₽</div>*/ ?>
        </div>
        <div class="catalog-item-basket btn-main">
            <div class="add-to-basket-react" data-is-exist="<?= BasketService::inBasket($item['ID']) ?>" data-product-id="<?= $item['ID']; ?>"></div>
        </div>
    </div>
</div>