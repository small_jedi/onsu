<?php use Bitrix\Main\Localization\Loc; ?>
    <h1>Избранные товары</h1>
<?php if ($arResult['ITEMS']): ?>
    <div class="site-list">
        <div class="site-list-row site-list-header">
            <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_NUMBER'); ?></div>
            <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_NAME'); ?></div>
            <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_PRICE'); ?></div>
            <div class="site-list-col"></div>
        </div>


        <?php foreach ($arResult['ITEMS'] as $item): ?>
            <div class="site-list-row">
                <div class="site-list-col">
                    <img style="width: 100px;" src="<?= $item['IMAGE']['SRC'] ?>">
                </div>
                <div class="site-list-col"><?= $item['ELEMENT_NAME']; ?></div>
                <div class="site-list-col"><?= $item['PRICE']; ?></div>
                <div class="site-list-col">
                    <div class="add-to-basket-react" data-product-id="<?= $item['ID']; ?>"></div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
<?php else: ?>
    <?= Loc::getMessage('GOOD_BEGIN_EMPTY_FAVORITE'); ?>
<?php endif; ?>