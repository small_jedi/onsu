<?php

use Bitrix\Main\Localization\Loc;

$orders = $arResult['ORDERS'];
?>
<?php if ($orders): ?>

    <div class="profile-short-info-container">
        <div class="profile-short-info">

            <?php foreach ($orders as $order): ?>
                <div class="profile-short-info-item">
                    <div class="profile-short-info-item-key"><?= Loc::getMessage('GOOD_BEGIN_PROFILE_ORDERS_NAME') ?><?= $order['ID']; ?></div>
                    <div class="profile-short-info-item-value"><?= $order['PRICE']; ?></div>
                </div>
            <?php endforeach; ?>

        </div>


        <a class="profile-short-info-detail" href="/profile/orders/"><?= Loc::getMessage('GOOD_BEGIN_PROFILE_ORDERS_ALL'); ?></a>
    </div>

<?php endif; ?>
