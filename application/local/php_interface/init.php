<?php
include_once 'constants.php';


if (!\Bitrix\Main\Loader::includeModule(DEV_MODULE_ID)) throw new Exception('Модуль ' . DEV_MODULE_ID . ' не подключен');



//old bitrix/php_interface/init.php
/**
 * Created by PhpStorm.
 * User: Gredasow Iwan (Griff)
 * Date: 28.05.2019
 * Time: 12:26
 */
//use Bitrix\Main\Loader;
//use Bitrix\Sale\DiscountCouponsManager;
//Loader::includeModule('sale');
//
//addEventHandler('sale', DiscountCouponsManager::EVENT_ON_COUPON_APPLY, 'onCouponApplyHandler');
//function onCouponApplyHandler($coupon) {
//    file_put_contents('griff_log.txt', $coupon . "\n", FILE_APPEND);
//}