<?php
$MESS['LANG_PROFILE_SEF_PERSONAL'] = 'Просмотр профиля';
$MESS['LANG_PROFILE_SEF_EDIT'] = 'Редактирование профиля';
$MESS['LANG_PROFILE_SEF_ORDERS'] = 'Список заявок';
$MESS['LANG_PROFILE_SEF_ORDER'] = 'Просмотр заявки';
$MESS['LANG_PROFILE_SEF_SERVICE'] = 'Сервис';
$MESS['LANG_PROFILE_SEF_EQUIP'] = 'Оборудование';