<?php

use Bitrix\Main\Localization\Loc;

/* @var $this CBitrixComponentTemplate */


$APPLICATION->IncludeFile('/local/components/dev/profile/templates/.default/content.php');

$APPLICATION->AddChainItem(Loc::getMessage('GOOD_BEGIN_MENU_SETTINGS'), $arResult['SEF_PARAMS']['FOLDER'] . $arResult['SEF_PARAMS']['URL_TEMPLATES']['orders']);
$APPLICATION->SetTitle(Loc::getMessage('GOOD_BEGIN_MENU_SETTINGS'));


$this->SetViewTarget(SET_VIEW_TARGET_PROFILE_CONTENT);

global $USER;
$APPLICATION->IncludeComponent("bitrix:forum.user.profile.edit", "custom", Array(
    "CACHE_TIME" => "0",    // Время кеширования (сек.)
    "CACHE_TYPE" => "A",    // Тип кеширования
    "SET_NAVIGATION" => "Y",    // Показывать навигацию
    "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
    "UID" => $USER->GetID(),    // ID пользователя
    "URL_TEMPLATES_PROFILE_VIEW" => "profile_view.php?UID=#UID#",    // Страница просмотра профиля
    "USER_PROPERTY" => "",    // Показывать доп. свойства
),
    false
);

$this->EndViewTarget(); ?>