<?php

/* @var $this CBitrixComponentTemplate */

use Bitrix\Main\Localization\Loc;

$APPLICATION->IncludeFile('/local/components/dev/profile/templates/.default/content.php');
$APPLICATION->AddChainItem(Loc::getMessage('GOOD_BEGIN_MENU_ORDERS'));

$this->SetViewTarget(SET_VIEW_TARGET_PROFILE_CONTENT);

global $USER;
$APPLICATION->IncludeComponent(
    "dev:profile.orders",
    ".default",
    array(
        "USER_ID" => $USER->GetId(),
        "COMPONENT_TEMPLATE" => ".default",
        "LIMIT" => "20"
    ),
    false
);

$this->EndViewTarget();

?>