<?php

/* @var $this CBitrixComponentTemplate */

use Bitrix\Main\Localization\Loc;

$APPLICATION->IncludeFile('/local/components/dev/profile/templates/.default/content.php');
$APPLICATION->AddChainItem(Loc::getMessage('GOOD_BEGIN_MENU_ORDERS'), $arResult['SEF_PARAMS']['FOLDER'] . $arResult['SEF_PARAMS']['URL_TEMPLATES']['orders']);
$APPLICATION->AddChainItem(Loc::getMessage('GOOD_BEGIN_MENU_ORDER_VIEW'));

$this->SetViewTarget(SET_VIEW_TARGET_PROFILE_CONTENT);
$APPLICATION->IncludeComponent(
    "dev:profile.orders.detail",
    ".default",
    array(
        "ORDER_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
        "COMPONENT_TEMPLATE" => ".default",
        "PROPERTIES" => array(
            0 => "1",
            1 => "2",
            2 => "3",
            3 => "4",
            4 => "5",
            5 => "6",
            6 => "7",
            7 => "9",
        )
    ),
    false
);
$this->EndViewTarget();


?>