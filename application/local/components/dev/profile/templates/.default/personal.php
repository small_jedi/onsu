<?php
/* @var $this CBitrixComponentTemplate */
$APPLICATION->IncludeFile('/local/components/dev/profile/templates/.default/content.php');


$this->SetViewTarget(SET_VIEW_TARGET_PROFILE_RIGHT_SB); ?>
    <div class="profile-right-side">
        <?php
        $APPLICATION->IncludeComponent("dev:profile.orders", "last_five", Array(
            "USER_ID" => $USER->GetId(),    // ID пользователя
            "COMPONENT_TEMPLATE" => ".default",
            "LIMIT" => "5",    // Количество элементов
        ),
            false
        );
        ?>

        <div class="profile-sales-container">
            <div class="swiper profile-sales">
                <div class="swiper-wrapper">
                    <div class="profile-sales-item swiper-slide"><img src="/local/assets/images/sales/square/1.png"></div>
                    <div class="profile-sales-item swiper-slide"><img src="/local/assets/images/sales/square/1.png"></div>
                </div>
            </div>
        </div>
    </div>
<?php $this->EndViewTarget(); ?>