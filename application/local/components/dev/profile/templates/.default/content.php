<?php

/* @var $this CBitrixComponentTemplate */

use Bitrix\Main\Localization\Loc;

?>
<div class="profile-container">
    <div class="profile-left-side">

        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "profile",
            array(
                "COMPONENT_TEMPLATE" => "profile",
                "ROOT_MENU_TYPE" => "left",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(),
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            ),
            false
        );
        ?>


        <div class="profile-resume">
            <div class="profile-resume-avatar"><img class="profile-resume-avatar__image" src="/local/assets/images/avatar.jpg"></div>
            <a class="profile-resume-logout" href="/profile/logout/"><?= Loc::getMessage('GOOD_BEGIN_LOGOUT'); ?></a></div>
    </div>


    <div class="profile"><h1 class="page-title"><?php $APPLICATION->ShowTitle(); ?></h1>
        <div class="profile-cards">
            <div class="profile-cards-item">
                <div class="profile-cards-item__icon"></div>
                <div class="profile-cards-item-data">
                    <div class="profile-cards-item__name"></div>
                    <div class="profile-cards-item__info"></div>
                </div>
            </div>
        </div>


        <?php $APPLICATION->ShowViewContent(SET_VIEW_TARGET_PROFILE_CONTENT); ?>

    </div>


    <?php $APPLICATION->ShowViewContent(SET_VIEW_TARGET_PROFILE_RIGHT_SB); ?>
</div>