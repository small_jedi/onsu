<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentDescription = array(
    "NAME" => \Bitrix\Main\Localization\Loc::getMessage('LANG_COMP_NAME'),
    "DESCRIPTION" => \Bitrix\Main\Localization\Loc::getMessage('LANG_COMP_DESC'),
);
?>