<?php


class UserProfile extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    protected $componentPage = 'personal';
    protected $arDefaultVariableAliases404 = array();
    protected $arComponentVariables = array(
        "SECTION_ID",
        "SECTION_CODE",
        "ELEMENT_ID",
        "ELEMENT_CODE",
    );
    protected $arDefaultUrlTemplates404 = array(
        "personal" => "/profile/",
        "orders" => "/profile/orders/",
        "order" => "/profile/orders/#ORDER_ID#/",
    );

    public function executeComponent()
    {
        $this->sefParse();
        $this->includeComponentTemplate($this->componentPage);
    }

    private function sefParse()
    {
        global $APPLICATION;
        $arParams = $this->arParams;
        $arDefaultUrlTemplates404 = $this->arDefaultUrlTemplates404;
        $arDefaultVariableAliases404 = $this->arDefaultVariableAliases404;
        $arComponentVariables = $this->arComponentVariables;
        $arVariables = array();

        $arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
        $arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);


        $engine = new CComponentEngine($this);
        if (CModule::IncludeModule('iblock')) {
            $engine->addGreedyPart("#SECTION_CODE_PATH#");
            $engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
        }
        $componentPage = $engine->guessComponentPath(
            $arParams["SEF_FOLDER"],
            $arUrlTemplates,
            $arVariables
        );

        $b404 = false;
        if ($componentPage) $this->componentPage = $componentPage;

//        if ($componentPage == "section") {
//            if (isset($arVariables["SECTION_ID"]))
//                $b404 |= (intval($arVariables["SECTION_ID"]) . "" !== $arVariables["SECTION_ID"]);
//            else
//                $b404 |= !isset($arVariables["SECTION_CODE"]);
//        }

//        if ($b404 && CModule::IncludeModule('iblock')) {
//            $folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
//            if ($folder404 != "/")
//                $folder404 = "/" . trim($folder404, "/ \t\n\r\0\x0B") . "/";
//            if (mb_substr($folder404, -1) == "/")
//                $folder404 .= "index.php";
//
//            if ($folder404 != $APPLICATION->GetCurPage(true)) {
//                \Bitrix\Iblock\Component\Tools::process404(
//                    ""
//                    , ($arParams["SET_STATUS_404"] === "Y")
//                    , ($arParams["SET_STATUS_404"] === "Y")
//                    , ($arParams["SHOW_404"] === "Y")
//                    , $arParams["FILE_404"]
//                );
//            }
//        }

//        CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
//

        $this->arResult['SEF_PARAMS'] = array(
            "FOLDER" => $arParams["SEF_FOLDER"],
            "URL_TEMPLATES" => $arUrlTemplates,
            "VARIABLES" => $arVariables,
            "ALIASES" => $arVariableAliases,
        );
    }
}