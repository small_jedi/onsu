<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "SEF_MODE" => Array(
            "personal" => array(
                "NAME" => GetMessage("LANG_PROFILE_SEF_PERSONAL"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "edit" => array(
                "NAME" => GetMessage("LANG_PROFILE_SEF_EDIT"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "orders" => array(
                "NAME" => GetMessage("LANG_PROFILE_SEF_ORDERS"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "order" => array(
                "NAME" => GetMessage("LANG_PROFILE_SEF_ORDER"),
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
        ),
    ),
);
?>