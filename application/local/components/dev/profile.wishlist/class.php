<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class WishlistComponent extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    public function executeComponent()
    {
        $this->findWhish();

        global $APPLICATION;
        $APPLICATION->SetTitle(Loc::getMessage('GOOD_BEGIN_MODULE_PAGE_TITLE'));
        $this->includeComponentTemplate();
    }

    private function findWhish()
    {
        if (!Loader::includeModule(DEV_MODULE_ID)) throw new Exception(Loc::getMessage('GOOD_BEGIN_MODULE_NOT_INSTALLED'));
        if (!Loader::includeModule('catalog')) throw new Exception(Loc::getMessage('GOOD_BEGIN_MODULE_CATALOG_NOT_INSTALLED'));
        $list = \Good\Begin\Classes\Services\FavoriteService::getAll();


        $catalog_items = CCatalogProduct::GetList(array(), array(
            'ID' => $list
        ));

        $iblock_items = array();
        $_iblock_items = CIBlockElement::GetList(array(), array('IBLOCK_ID' => CATALOG_IBLOCK_ID, 'ID' => $list));
        while ($el = $_iblock_items->GetNext()) {
            $iblock_items[] = $el;
        }

        while ($el = $catalog_items->GetNext()) {

            $_item = array();
            foreach ($iblock_items as $item) {
                if ($item['ID'] == $el['ID']) $_item = $item;
            }

            $image_id = $_item['DETAIL_PICTURE'] ? $_item['DETAIL_PICTURE'] : $_item['PREVIEW_PICTURE'];
            $price = CPrice::GetBasePrice($el['ID']);
            $_el = array(
                'ID' => $el['ID'],
                'ELEMENT_NAME' => $el['ELEMENT_NAME'],
                'PRICE' => $price['PRICE'],
            );

            if ($image_id) $_el['IMAGE'] = CFile::GetFileArray($image_id);

            $this->arResult['ITEMS'][] = $_el;
        }
    }
}