<?php

use Bitrix\Main\Localization\Loc;

class ProfileOrdersDetail extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        global $USER;


        $arParams['ORDER_ID'] = $arParams['ORDER_ID'] ? intval($arParams['ORDER_ID']) : $USER->GetID();

        return $arParams;
    }

    public function executeComponent()
    {
        if (!CModule::IncludeModule(DEV_MODULE_ID)) throw new Exception(Loc::getMessage('GOOD_BEGIN_MODULE_NOT_INCLUDE'));


        $this->findOrders();
        $this->findOrderProperties();

        $this->includeComponentTemplate();
    }

    private function findOrders()
    {
        $this->arResult['ORDER'] = \Good\Begin\Classes\Orders::getOne($this->arParams['ORDER_ID']);
    }

    private function findOrderProperties()
    {
        $collection = $this->arResult['ORDER']->getPropertyCollection();
        foreach ($this->arParams['PROPERTIES'] as $property_id) {
            if ($property = $collection->getItemByOrderPropertyId($property_id)) $this->arResult['PROPERTIES'][] = $property;
        }
    }
}