<?php
$MESS['GOOD_BEGIN_LIST_ITEMS'] = 'Список товарв в заказе';
$MESS['GOOD_BEGIN_LIST_F_NAME'] = 'Название товара';
$MESS['GOOD_BEGIN_LIST_F_TITLE'] = 'Заказ №';
$MESS['GOOD_BEGIN_LIST_F_TITLE_FR'] = ' от ';
$MESS['GOOD_BEGIN_LIST_F_QUANTITY'] = 'Количество';
$MESS['GOOD_BEGIN_LIST_F_WEIGHT'] = 'Вес';
$MESS['GOOD_BEGIN_LIST_F_PRICE'] = 'Цена';
$MESS['GOOD_BEGIN_ORDER_DETAIL_NUMBER_F'] = 'Номер заказа:';
$MESS['GOOD_BEGIN_ORDER_DETAIL_STATUS_F'] = 'Статус заказа:';
$MESS['GOOD_BEGIN_ORDER_DETAIL_AMOUNT_F'] = 'Стоимость:';
$MESS['GOOD_BEGIN_ORDER_DETAIL_DATE_F'] = 'Дата покупки:';
$MESS['GOOD_BEGIN_ORDER_PAID'] = 'Заказ оплачен';
$MESS['GOOD_BEGIN_ORDER_NO_PAID'] = 'Заказ не оплачен';
?>