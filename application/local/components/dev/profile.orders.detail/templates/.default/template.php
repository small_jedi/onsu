<?php

use Bitrix\Main\Localization\Loc;

/* @var $order \Bitrix\Sale\Order
 * @var $items \Bitrix\Sale\Basket[]
 * @var $properties Bitrix\Sale\PropertyValueCollection
 */
$order = $arResult['ORDER'];
$properties = $arResult['PROPERTIES'];
$APPLICATION->SetTitle(Loc::getMessage('GOOD_BEGIN_LIST_F_TITLE') . $order->getId() . Loc::getMessage('GOOD_BEGIN_LIST_F_TITLE_FR') . $order->getField('DATE_INSERT'));
$items = $order->getBasket();
?>

<div class="profile-order-detail">
    <?php if ($order->isPaid()): ?>
        <div class="info-block succ"><?= Loc::getMessage('GOOD_BEGIN_ORDER_PAID'); ?></div>
    <?php else: ?>
        <div class="info-block error"><?= Loc::getMessage('GOOD_BEGIN_ORDER_NO_PAID'); ?></div>
    <?php endif; ?>

    <div class="profile-order-detail-content">
        <div class="order-detail-info profile-order-detail-panel">
            <div><?= Loc::getMessage('GOOD_BEGIN_ORDER_DETAIL_NUMBER_F'); ?>&nbsp;<?= $order->getId(); ?></div>
            <div><?= Loc::getMessage('GOOD_BEGIN_ORDER_DETAIL_STATUS_F'); ?>&nbsp;<?= $order->getField('STATUS_ID'); ?></div>
            <div><?= Loc::getMessage('GOOD_BEGIN_ORDER_DETAIL_AMOUNT_F'); ?>&nbsp;<?= $order->getPrice(); ?></div>
            <div><?= Loc::getMessage('GOOD_BEGIN_ORDER_DETAIL_DATE_F'); ?>&nbsp;<?= $order->getField('DATE_INSERT'); ?></div>
        </div>
        <div class="order-detail-buyer profile-order-detail-panel">
            <?php foreach ($properties as $property): ; ?>
                <?php $prop_val = $property->getValue(); ?>
                <?php if (strlen($prop_val) > 0): ?>
                    <div><?= $property->getName(); ?>: <?= $property->getValue(); ?></div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="profile-order-detail-content">

        <h2><?= Loc::getMessage('GOOD_BEGIN_LIST_ITEMS') ?></h2>
        <div class="site-list">
            <div class="site-list-row site-list-header">
                <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_NAME'); ?></div>
                <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_QUANTITY'); ?></div>
                <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_WEIGHT'); ?></div>
                <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_PRICE'); ?></div>
            </div>

            <?php foreach ($items as $item): ?>
                <div class="site-list-row">
                    <div class="site-list-col"><?= $item->getField('NAME'); ?>(<?= $item->getProductId(); ?>)</div>
                    <div class="site-list-col"><?= $item->getQuantity(); ?></div>
                    <div class="site-list-col"><?= $item->getWeight(); ?></div>
                    <div class="site-list-col"><?= $item->getPrice(); ?></div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>
