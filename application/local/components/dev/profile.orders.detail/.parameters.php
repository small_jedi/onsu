<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$order_properties = array();
if (\Bitrix\Main\Loader::includeModule('sale')) {
    $property_result = \Bitrix\Sale\Property::getList()->fetchAll();
    foreach ($property_result as $property) {
        $order_properties[$property['ID']] = $property['NAME'];
    }
}


global $USER;
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "ORDER_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("GOOD_BEGIN_DETAIL_ORDER_F"),
            "TYPE" => "STRING",
        ),
    ),
);

if ($order_properties) {
    $arComponentParameters['PARAMETERS']['PROPERTIES'] = array(
        "PARENT" => "BASE",
        "NAME" => GetMessage("GOOD_BEGIN_DETAIL_PROPERTIES_F"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $order_properties,
    );
}
?>