<?php

use Bitrix\Main\Localization\Loc;

$orders = $arResult['ORDERS'];
?>
<?php if ($orders): ?>
    <div class="site-list">
        <div class="site-list-row site-list-header">
            <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_NUMBER'); ?></div>
            <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_DATE'); ?></div>
            <div class="site-list-col"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_SUMMARY'); ?></div>
            <div class="site-list-col"></div>
        </div>


        <?php foreach ($orders as $order): ?>

            <div class="site-list-row">
                <div class="site-list-col">№<?= $order['ID']; ?></div>
                <div class="site-list-col"><?= $order['DATE_INSERT']; ?></div>
                <div class="site-list-col"><?= $order['PRICE']; ?></div>
                <div class="site-list-col">
                    <a class="btn-main" href="/profile/orders/<?= $order['ID']; ?>/"><?= Loc::getMessage('GOOD_BEGIN_LIST_F_DETAIL'); ?></a>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
<?php else: ?>
    <?= Loc::getMessage('GOOD_BEGIN_EMPTY_ORDERS'); ?>
<?php endif; ?>
