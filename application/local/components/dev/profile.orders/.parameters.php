<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "USER_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("GOOD_BEGIN_PROFILE_ORDERS_USER_ID_F"),
            "TYPE" => "STRING",
            "DEFAULT" => $USER->GetID(),
        ),
        "LIMIT" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("GOOD_BEGIN_PROFILE_ORDERS_LIMIT_F"),
            "TYPE" => "STRING",
            "DEFAULT" => 20,
        ),
    ),
);
?>