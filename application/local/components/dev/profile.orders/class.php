<?php

use Bitrix\Main\Localization\Loc;

class ProfileOrders extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        global $USER;

        $arParams['USER_ID'] = $arParams['USER_ID'] ? intval($arParams['USER_ID']) : $USER->GetID();
        $arParams['LIMIT'] = intval($arParams['LIMIT']);

        return $arParams;
    }

    public function executeComponent()
    {
        if (!CModule::IncludeModule('good.begin')) throw new Exception(Loc::getMessage('GOOD_BEGIN_MODULE_NOT_INCLUDE'));


        $this->findOrders();
        $this->includeComponentTemplate();
    }

    private function findOrders()
    {
        $this->arResult['ORDERS'] = \Good\Begin\Classes\Orders::getListByUserId($this->arParams['USER_ID'], $this->arParams['LIMIT']);
    }
}