<?php

use Good\Begin\Model\Location;

/**
 * @property Location[] $locations
 * */
class CityHeader extends CBitrixComponent
{
    private $locations;

    public function onPrepareComponentParams($arParams)
    {
    }

    public function executeComponent()
    {
        $this->getLocations();

        $this->arResult['CITY_LIST'] = $this->locations;
        $this->arResult['PREVIEW_CITY_LIST'] = array_slice($this->locations, 0, 10);

        $this->includeComponentTemplate();
    }

    private function getLocations()
    {
        $this->locations = Location::findAll();
    }
}