<?php

namespace Good\Begin\Rest\Models;

use Good\Begin\Rest\Interfaces\IRest;

class User implements IRest
{
    public function actionIndex()
    {
        $data = [];

        $users = \CUser::GetList($by = '', $order = '', array(), array());

        while ($user = $users->Fetch()) {
            $data[] = $user;
        }

        return $data;
    }

    public function actionCreate($params)
    {
        // TODO: Implement actionCreate() method.
    }

    public function actionUpdate($id, $params)
    {
        // TODO: Implement actionUpdate() method.
    }

    public function actionDelete($id)
    {
        // TODO: Implement actionDelete() method.
    }

    public function actionView($id)
    {

        $data = [];

        $users = \CUser::GetList($by = '', $order = '', array('ID' => $id), array());

        while ($user = $users->Fetch()) {
            $data = $user;
        }

        return $data;
    }
}