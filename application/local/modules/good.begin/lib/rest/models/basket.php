<?php

namespace Good\Begin\Rest\Models;

use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Fuser;
use Bitrix\Main\Context;
use Bitrix\Currency\CurrencyManager;
use Good\Begin\Rest\Interfaces\IRest;

if (!\Bitrix\Main\Loader::includeModule(DEV_MODULE_ID)) throw new Exception('Модуль ' . DEV_MODULE_ID . ' не подключен');

class Basket implements IRest
{
    public function actionIndex()
    {
        // TODO: Implement actionIndex() method.
    }

    public function actionCreate($params)
    {
        if (!\CModule::IncludeModule('sale')) throw new \Exception(Loc::getMessage('GOOD_BEGIN_LIB_BASKET_NO_MODULE'));
        if (!array_key_exists('product_id', $params)) throw new \Exception(Loc::getMessage('GOOD_BEGIN_LIB_BASKET_EMPTY_PRODUCT_ID_PARAM'));
        $product_id = intval($params['product_id']);
        if (!is_numeric($product_id) || !is_int($product_id)) throw new \Exception(Loc::getMessage('GOOD_BEGIN_LIB_BASKET_PARAM_NO_NUMERIC'));
        $site_id = Context::getCurrent()->getSite();


        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(Fuser::getId(), Context::getCurrent()->getSite());
        if ($item = $basket->getExistsItem('catalog', $product_id)) {
            $item->setField('QUANTITY', $item->getQuantity() + 1);
        } else {
            $item = $basket->createItem('catalog', $product_id);
            $result_set_fields = $item->setFields(array(
                'QUANTITY' => 1,
                'CURRENCY' => CurrencyManager::getBaseCurrency(),
                'LID' => $site_id,
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            ));

            $result_add = $basket->addItem($item);
        }

        $result_save = $basket->save();


        return [
            'status' => 200,
        ];
    }

    public function actionUpdate($id, $params)
    {
        // TODO: Implement actionUpdate() method.
    }

    public function actionDelete($id)
    {
        // TODO: Implement actionDelete() method.
    }

    public function actionView($id)
    {
        // TODO: Implement actionView() method.
    }
}