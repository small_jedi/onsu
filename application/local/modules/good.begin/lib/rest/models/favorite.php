<?php


namespace Good\Begin\Rest\Models;

use Good\Begin\Classes\Services\FavoriteService;
use Good\Begin\Rest\Interfaces\IRest;

if (!\Bitrix\Main\Loader::includeModule(DEV_MODULE_ID)) throw new Exception('Модуль ' . DEV_MODULE_ID . ' не подключен');

class Favorite implements IRest
{

    public function actionIndex()
    {
        // TODO: Implement actionIndex() method.
    }

    public function actionCreate($params)
    {
        $product_id = $params['product_id'];

        FavoriteService::add($product_id);

        return FavoriteService::isExist($product_id);
    }

    public function actionUpdate($id, $params)
    {
        // TODO: Implement actionUpdate() method.
    }

    public function actionDelete($id)
    {
        // TODO: Implement actionDelete() method.
    }

    public function actionView($id)
    {
        // TODO: Implement actionView() method.
    }
}