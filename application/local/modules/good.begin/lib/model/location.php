<?php

namespace Good\Begin\Model;

use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

/**
 * @property string $name
 * @property string $code
 */
class Location
{
    public $name;
    public $code;

    public static function findAll()
    {
        $models = array();

        Loader::includeModule('sale');

        $res = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array('=TYPE.ID' => '5', '=NAME.LANGUAGE_ID' => LANGUAGE_ID),
            'select' => array('NAME_RU' => 'NAME.NAME')
        ));

        while ($item = $res->fetch()) {

//            Debug::dumpToFile($item);

            $_this = new Location();
            $_this->name = $item['NAME_RU'];

            $models[] = $_this;
        }

        return $models;
    }
}