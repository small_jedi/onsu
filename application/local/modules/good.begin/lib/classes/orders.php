<?php

namespace Good\Begin\Classes;

use Bitrix\Sale\Order;

class Orders
{
    public static function getListByUserId(int $user_id, int $limit = 0)
    {
        if (!\CModule::IncludeModule('sale')) return false;


        $parameters['filter'] = array(
            'USER_ID' => $user_id,
        );

        if ($limit > 0) $parameters['limit'] = $limit;

        $orders = Order::getList($parameters)->fetchAll();

        return $orders;
    }

    public static function getOne(int $order_id)
    {
        if (!\CModule::IncludeModule('sale')) return false;

        return Order::load($order_id);
    }
}