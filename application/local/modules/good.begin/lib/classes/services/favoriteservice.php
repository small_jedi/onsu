<?php

namespace Good\Begin\Classes\Services;

use Bitrix\Main\Diag\Debug;

class FavoriteService
{
    const KEY = 'FAVORITE';

    public static function add(int $product_id)
    {
        $_SESSION[self::KEY][$product_id] = $product_id;
    }

    public static function remove(int $product_id)
    {
        unset($_SESSION[self::KEY][$product_id]);
    }

    public static function isExist($product_id)
    {
        return array_key_exists($product_id, $_SESSION[self::KEY]) ? 'true' : 'false';
    }

    public static function clear()
    {
        unset($_SESSION[self::KEY]);
    }

    public static function getAll()
    {
        return $_SESSION[self::KEY];
    }

    public static function count()
    {
        return count($_SESSION[self::KEY]);
    }
}