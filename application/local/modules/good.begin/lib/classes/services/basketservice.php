<?php


namespace Good\Begin\Classes\Services;


use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Fuser;

class BasketService
{
    public static function inBasket(int $product_id)
    {
        if (!Loader::includeModule('sale')) throw new \Exception('Модуль sale не подключен.');
        $basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
        $exist = $basket->getBasket()->getExistsItem('catalog', $product_id);

        foreach ($basket->getBasket() as $e) {
//            Debug::dumpToFile($e->getId());
        }

        return $exist ? 'true' : 'false';
    }

    /**
     * @return integer
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\NotImplementedException
     */
    public static function count()
    {
        if (!Loader::includeModule('sale')) throw new \Exception('Модуль sale не подключен.');
        $basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
        return $basket->count();
    }
}