<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;

class good_begin extends CModule
{
    var $MODULE_ID = "good.begin";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;
    var $folder_storage = 'local';

    public function __construct()
    {
        $arModuleVersion = array();
        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = '0.0.1';
            $this->MODULE_VERSION_DATE = '2022-01-1 11:00:00';
        }

        //settings
        $this->MODULE_NAME = Loc::getMessage('GOOD_BEGIN_MODULE_NAME');
        $this->PARTNER_NAME = 'Developer';
    }


    function DoInstall()
    {
        global $DB, $DBType, $APPLICATION;
        ModuleManager::registerModule($this->MODULE_ID);
        if (\Bitrix\Main\Loader::includeModule($this->MODULE_ID)) {
            $this->InstallFiles();
            $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/" . $DBType . "/install.sql");
        }
    }

    function DoUninstall()
    {
        global $DB, $DBType, $APPLICATION;
        ModuleManager::unRegisterModule($this->MODULE_ID);
        $this->UnInstallFiles();
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/" . $DBType . "/uninstall.sql");
    }


    function InstallDB()
    {
        return true;
    }

    function UnInstallDB($arParams = array())
    {
        return true;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/{$this->folder_storage}/modules/{$this->MODULE_ID}/install/admin/page", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/page", true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/{$this->folder_storage}/modules/{$this->MODULE_ID}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/" . $this->folder_storage . "/components", true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/{$this->folder_storage}/modules/{$this->MODULE_ID}/install/admin/page", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/page");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/{$this->folder_storage}/modules/{$this->MODULE_ID}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/" . $this->folder_storage . "/components");
        return true;
    }


}