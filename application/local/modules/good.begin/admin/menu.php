<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$menu = array(
    array(
        'parent_menu' => 'global_menu_services',//определяем место меню, в данном случае оно находится в главном меню
        'sort' => 400,//сортировка, в каком месте будет находится наш пункт
        'text' => Loc::getMessage('GOOD_BEGIN_MENU_TITLE'),//описание из файла локализации
        'title' => Loc::getMessage('GOOD_BEGIN_MENU_TITLE'),//название из файла локализации
        'url' => 'page/good_begin.php',//ссылка на страницу из меню
        'items_id' => 'menu_references',//описание подпункта, то же, что и ранее, либо другое, можно вставить сколько угодно пунктов меню
        'items' => array(
//            array(
//                'text' => Loc::getMessage('GOOD_BEGIN_POINT_EQUIP_TITLE'),
//                'url' => 'page/good_begin_points.php?lang=' . LANGUAGE_ID,
//                'more_url' => array('good_begin.php?lang=' . LANGUAGE_ID),
//                'title' => Loc::getMessage('GOOD_BEGIN_POINT_EQUIP_TITLE'),
//            ),
        ),
    ),
);

return $menu;