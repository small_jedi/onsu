<?php
$MESS["LANDING_CMP_NOT_INSTALLED"] = "Usługa strony docelowej jest obecnie niedostępna";
$MESS["LANDING_ERROR_ACCESS_DENIED"] = "Odmowa dostępu";
$MESS["LANDING_ERROR_SESS_EXPIRED"] = "Twoja sesja wygasła.";
$MESS["LANDING_GOTO_EDIT"] = "Edytuj ustawienia";
