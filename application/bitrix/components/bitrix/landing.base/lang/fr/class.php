<?php
$MESS["LANDING_CMP_NOT_INSTALLED"] = "Le service page de destination est actuellement indisponible";
$MESS["LANDING_ERROR_ACCESS_DENIED"] = "Accès refusé";
$MESS["LANDING_ERROR_SESS_EXPIRED"] = "Votre session a expiré.";
$MESS["LANDING_GOTO_EDIT"] = "Modifier les paramètres";
