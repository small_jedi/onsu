<?php
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "Não foi possível criar um dicionário com este nome";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Adicionar itens à lista do dicionário";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Novo produto";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "O produto não foi encontrado. Pode ter sido excluído.";
