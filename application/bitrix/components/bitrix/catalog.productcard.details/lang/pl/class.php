<?php
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "Nie można utworzyć słownika o tej nazwie";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Dodaj elementy do listy słowników";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Nowy produkt";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "Nie znaleziono produktu. Być może został usunięty.";
