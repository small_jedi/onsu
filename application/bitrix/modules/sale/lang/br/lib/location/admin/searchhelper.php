<?
$MESS["SALE_LOCATION_ADMIN_SEARCH_HELPER_ENTITY_INVALID_SINDEX"] = "O índice de pesquisa de locais está desatualizado. Se você usar a pesquisa incremental na caixa de entrada, pode ser lento, os resultados podem ser irrelevantes. É recomendável que você #ANCHOR_INDEX_RESTORE# restaure o índice manualmente #ANCHOR_END#.";
?>