<?php
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_LESS"] = "Ya se agregaron al carrito #QUANTITY# pzas de \"#PRODUCT_NAME#\". Puede agregar hasta #ADD# más.";
$MESS["SALE_BASKET_AVAILABLE_FOR_ADDING_QUANTITY_IS_ZERO"] = "La cantidad máxima disponible (#QUANTITY#) de \"#PRODUCT_NAME#\" ya se agregó al carrito.";
$MESS["SALE_BASKET_AVAILABLE_FOR_DECREASE_QUANTITY"] = "Cantidad de \"#PRODUCT_NAME#\" disponible para deducción: #AVAILABLE_QUANTITY#";
$MESS["SALE_BASKET_AVAILABLE_FOR_PURCHASE_QUANTITY"] = "Cantidad de \"#PRODUCT_NAME#\" disponible para compra: #AVAILABLE_QUANTITY#";
$MESS["SALE_BASKET_ITEM_NOT_UPDATED_BECAUSE_NOT_EXISTS"] = "El producto \"#PRODUCT_NAME#\" no se actualizó porque se había eliminado anteriormente.";
$MESS["SALE_BASKET_ITEM_WRONG_AVAILABLE_QUANTITY"] = "Error al verificar la disponibilidad de \"#PRODUCT_NAME#\"";
