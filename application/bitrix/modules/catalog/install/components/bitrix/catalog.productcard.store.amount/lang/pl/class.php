<?php
$MESS["STORES_LIST_DROPDOWN_ACTIONS_LIST_DELETE"] = "Usuń";
$MESS["STORES_LIST_DROPDOWN_ACTIONS_LIST_NONE"] = "Wybierz działanie";
$MESS["STORE_LIST_GRID_HEADER_AMOUNT"] = "Kwota";
$MESS["STORE_LIST_GRID_HEADER_QUANTITY"] = "Dostępny asortyment";
$MESS["STORE_LIST_GRID_HEADER_QUANTITY_COMMON"] = "Ilość magazynowa";
$MESS["STORE_LIST_GRID_HEADER_QUANTITY_RESERVED"] = "Zarezerwowana";
$MESS["STORE_LIST_GRID_HEADER_TITLE"] = "Magazyn";
$MESS["STORE_LIST_GRID_IM_STUB_TITLE"] = "Aby zarządzać zapasami produktów, włącz Zarządzanie asortymentem.";
$MESS["STORE_LIST_GRID_STUB_TITLE"] = "Ten produkt nie jest dostępny w żadnym magazynie.";
