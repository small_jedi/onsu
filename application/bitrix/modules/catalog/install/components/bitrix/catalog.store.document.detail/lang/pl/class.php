<?php
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CANCEL_ERROR"] = "Błąd podczas anulowania przetwarzania";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CONDUCT_ERROR"] = "Błąd przetwarzania";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_NOT_FOUND_ERROR"] = "Nie znaleziono obiektu magazynowego.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_TYPE_NOT_SPECIFIED_ERROR"] = "Typ obiektu magazynowego jest wymagany.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_A"] = "Kwit składowy";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_D"] = "Odpis";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_M"] = "Transfer zasobu";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_VIEW_RIGHTS_ERROR"] = "Niewystarczające uprawnienia do wyświetlania obiektu magazynowego";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_WRITE_RIGHTS_ERROR"] = "Niewystarczające uprawnienia do zapisania obiektu magazynowego";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_SAVE_ERROR"] = "Błąd podczas zapisywania obiektu magazynowego";
