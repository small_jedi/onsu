<?php
$MESS["DOCUMENT_STATUS_CANCELLED"] = "Anulowano";
$MESS["DOCUMENT_STATUS_N"] = "Wersja robocza";
$MESS["DOCUMENT_STATUS_Y"] = "Przetworzono";
$MESS["DOCUMENT_TYPE_DROPDOWN"] = "#TYPE#";
$MESS["DOC_TYPE_SHORT_A"] = "Kwit składowy";
$MESS["DOC_TYPE_SHORT_D"] = "Odpis";
$MESS["DOC_TYPE_SHORT_M"] = "Transfer";
$MESS["DOC_TYPE_SHORT_S"] = "Korekta zapasów";
$MESS["TAB_GENERAL_TITLE"] = "Ogólne";
$MESS["TAB_PRODUCT_TITLE"] = "Produkty";
