<?php
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CANCEL_ERROR"] = "Error al cancelar el procesamiento";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CONDUCT_ERROR"] = "Error durante el procesamiento";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_NOT_FOUND_ERROR"] = "No se encontró el objeto del inventario.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_TYPE_NOT_SPECIFIED_ERROR"] = "Se requiere el tipo de objeto del inventario.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_A"] = "Recibo de existencias";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_D"] = "Cancelación";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_M"] = "Transferencia de existencias";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_VIEW_RIGHTS_ERROR"] = "Permisos insuficientes para ver el objeto del inventario";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_WRITE_RIGHTS_ERROR"] = "Permisos insuficientes para guardar los objetos del inventario";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_SAVE_ERROR"] = "Error al guardar el objeto del inventario";
