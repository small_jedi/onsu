<?php
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CANCEL_ERROR"] = "Erro ao cancelar o processamento";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CONDUCT_ERROR"] = "Erro de processamento";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_NOT_FOUND_ERROR"] = "O objeto de inventário não foi encontrado.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_TYPE_NOT_SPECIFIED_ERROR"] = "O tipo de objeto de inventário é obrigatório.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_A"] = "Recibo de estoque";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_D"] = "Baixa";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_M"] = "Transferência de estoque";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_VIEW_RIGHTS_ERROR"] = "Permissões insuficientes para visualizar o objeto de inventário";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_WRITE_RIGHTS_ERROR"] = "Permissões insuficientes para salvar o objeto de inventário";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_SAVE_ERROR"] = "Erro ao salvar objeto de inventário";
