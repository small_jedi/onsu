<?php
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CANCEL_ERROR"] = "Erreur lors de l'annulation du traitement";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_CONDUCT_ERROR"] = "Erreur lors du traitement";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_NOT_FOUND_ERROR"] = "L'objet d'inventaire est introuvable.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOCUMENT_TYPE_NOT_SPECIFIED_ERROR"] = "Le type d'objet d'inventaire est requis.";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_A"] = "Réception en stock";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_D"] = "Radiation";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_DOC_TYPE_M"] = "Transfert de stock";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_VIEW_RIGHTS_ERROR"] = "Droits insuffisants pour visualiser l'objet d'inventaire";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_NO_WRITE_RIGHTS_ERROR"] = "Droits insuffisants pour enregistrer l'objet d'inventaire";
$MESS["CATALOG_STORE_DOCUMENT_DETAIL_SAVE_ERROR"] = "Erreur lors de l'enregistrement de l'objet d'inventaire";
