<?php
$MESS["DOCUMENT_STATUS_CANCELLED"] = "Annulé";
$MESS["DOCUMENT_STATUS_N"] = "Brouillon";
$MESS["DOCUMENT_STATUS_Y"] = "Traité";
$MESS["DOCUMENT_TYPE_DROPDOWN"] = "#TYPE#";
$MESS["DOC_TYPE_SHORT_A"] = "Réception en stock";
$MESS["DOC_TYPE_SHORT_D"] = "Radiation";
$MESS["DOC_TYPE_SHORT_M"] = "Transférer";
$MESS["DOC_TYPE_SHORT_S"] = "Ajustement du stock";
$MESS["TAB_GENERAL_TITLE"] = "Général";
$MESS["TAB_PRODUCT_TITLE"] = "Produits";
