<?php
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_ADD_PRODUCT"] = "Adicionar linha";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_ADD_PRODUCT_TITLE"] = "Encontrar ou criar um novo produto";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_BARCODE_SETTING_PRODUCT"] = "Configurações de verificação";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_BARCODE_SETTING_PRODUCT_TITLE"] = "Configurações de verificação";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_COPY_ACTION"] = "Copiar";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_CREATE_PRODUCT"] = "Criar produto";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_CREATE_PRODUCT_TITLE"] = "Crie um novo produto do catálogo";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_DELETE_ACTION"] = "Excluir";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_NEGATIVE_PRICE_NOTICE"] = "O preço não pode ser negativo.";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_SETTING_DISABLED"] = "Parâmetro \"#NAME#\" desativado.";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_SETTING_ENABLED"] = "Parâmetro \"#NAME#\" ativado";
$MESS["CATALOG_PRODUCT_SUM_TOTAL"] = "Valor total";
