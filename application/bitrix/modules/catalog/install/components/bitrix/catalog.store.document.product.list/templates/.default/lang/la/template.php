<?php
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_ADD_PRODUCT"] = "Agregar fila";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_ADD_PRODUCT_TITLE"] = "Encontrar o crear un nuevo producto";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_BARCODE_SETTING_PRODUCT"] = "Configuración del escaneo";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_BARCODE_SETTING_PRODUCT_TITLE"] = "Configuración del escaneo";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_COPY_ACTION"] = "Copiar";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_CREATE_PRODUCT"] = "Crear un producto";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_CREATE_PRODUCT_TITLE"] = "Crear un nuevo producto del catálogo";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_DELETE_ACTION"] = "Eliminar";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_NEGATIVE_PRICE_NOTICE"] = "El precio no puede ser negativo.";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_SETTING_DISABLED"] = "Parámetro \"#NAME#\" deshabilitado.";
$MESS["CATALOG_DOCUMENT_PRODUCT_LIST_SETTING_ENABLED"] = "Parámetro \"#NAME#\" habilitado";
$MESS["CATALOG_PRODUCT_SUM_TOTAL"] = "Importe total";
