<?php
$MESS["DOCUMENT_LIST_ACTION_CANCEL_TEXT"] = "Anuluj przetwarzanie";
$MESS["DOCUMENT_LIST_ACTION_CANCEL_TITLE"] = "Anuluj przetwarzanie";
$MESS["DOCUMENT_LIST_ACTION_CONDUCT_TEXT"] = "Przetwórz";
$MESS["DOCUMENT_LIST_ACTION_CONDUCT_TITLE"] = "Przetwórz";
$MESS["DOCUMENT_LIST_ACTION_DELETE_TEXT"] = "Usuń";
$MESS["DOCUMENT_LIST_ACTION_DELETE_TITLE"] = "Usuń";
$MESS["DOCUMENT_LIST_ACTION_OPEN_TEXT"] = "Otwórz";
$MESS["DOCUMENT_LIST_ACTION_OPEN_TITLE"] = "Otwórz";
$MESS["DOCUMENT_LIST_ADD_DOCUMENT_BUTTON"] = "Dodaj";
$MESS["DOCUMENT_LIST_CANCEL_GROUP_ACTION"] = "Anuluj przetwarzanie";
$MESS["DOCUMENT_LIST_CONDUCT_GROUP_ACTION"] = "Przetwórz";
$MESS["DOCUMENT_LIST_DOC_TYPE_A"] = "Kwit składowy";
$MESS["DOCUMENT_LIST_DOC_TYPE_D"] = "Odpis";
$MESS["DOCUMENT_LIST_DOC_TYPE_DEFAULT"] = "Obiekt magazynowy";
$MESS["DOCUMENT_LIST_DOC_TYPE_M"] = "Transfer";
$MESS["DOCUMENT_LIST_DOC_TYPE_S"] = "Korekta zapasów";
$MESS["DOCUMENT_LIST_DOC_TYPE_W"] = "Zamówienia sprzedaży";
$MESS["DOCUMENT_LIST_NO_VIEW_RIGHTS_ERROR"] = "Niewystarczające uprawnienia do wyświetlania listy obiektów magazynowych.";
$MESS["DOCUMENT_LIST_SELECT_GROUP_ACTION"] = "Wybierz działanie";
$MESS["DOCUMENT_LIST_STATUS_CANCELLED"] = "Anulowano";
$MESS["DOCUMENT_LIST_STATUS_N"] = "Wersja robocza";
$MESS["DOCUMENT_LIST_STATUS_Y"] = "Przetworzono";
$MESS["DOCUMENT_LIST_STUB_DESCRIPTION_ARRIVAL"] = "Chcesz zarządzać asortymentem? Dzięki Bitrix24 wiesz, co jest w magazynie, gdzie znajduje się dana pozycja i co można zrobić lepiej.";
$MESS["DOCUMENT_LIST_STUB_DESCRIPTION_DEDUCT"] = "Odpisy pomagają śledzić stany magazynowe, minimalizować straty produktów i unikać kosztownych błędów";
$MESS["DOCUMENT_LIST_STUB_DESCRIPTION_MOVING"] = "Śledź i zarządzaj asortymentem w różnych magazynach, aby zapewnić dostępność produktów przez cały czas i zmaksymalizować wysiłki sprzedażowe";
$MESS["DOCUMENT_LIST_STUB_DESCRIPTION_SHIPMENT"] = "Pozycja sprzedana? Dzięki zarządzaniu asortymentem Bitrix24 nigdy nie zabraknie Ci zapasów i będziesz w stanie szybko i sprawnie realizować zamówienia produktów.";
$MESS["DOCUMENT_LIST_STUB_LINK_CONTROL"] = "Jak pracować z transferami?";
$MESS["DOCUMENT_LIST_STUB_LINK_LOSSES"] = "Jak pracować z odpisami?";
$MESS["DOCUMENT_LIST_STUB_LINK_MIGRATION"] = "Używasz już oprogramowania magazynowego? Przenieś swoje dane z innych systemów.";
$MESS["DOCUMENT_LIST_STUB_LINK_QUICKSTART"] = "Jak rozpocząć zarządzanie asortymentem?";
$MESS["DOCUMENT_LIST_STUB_LINK_SHIPMENT"] = "Jak pracować z zamówieniami sprzedaży?";
$MESS["DOCUMENT_LIST_STUB_NO_DATA_DESCRIPTION"] = "Spróbuj zresetować filtr lub zmień wyszukiwaną frazę.";
$MESS["DOCUMENT_LIST_STUB_NO_DATA_TITLE"] = "Wyszukiwanie nie zwróciło wyników.";
$MESS["DOCUMENT_LIST_STUB_TITLE_ARRIVAL"] = "Utwórz swój pierwszy kwit składowy";
$MESS["DOCUMENT_LIST_STUB_TITLE_DEDUCT"] = "Dodaj odpis";
$MESS["DOCUMENT_LIST_STUB_TITLE_MOVING"] = "Utwórz swój pierwszy transfer";
$MESS["DOCUMENT_LIST_STUB_TITLE_SHIPMENT"] = "Utwórz swoje pierwsze zamówienie sprzedaży";
$MESS["DOCUMENT_LIST_TITLE_DOCUMENT_DATE"] = "z #DATE#";
