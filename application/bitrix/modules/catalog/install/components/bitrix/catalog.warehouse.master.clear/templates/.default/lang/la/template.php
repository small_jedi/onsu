<?php
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_1"] = "Habilitar la administración del inventario";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_2"] = "Deshabilitar la administración del inventario";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_3"] = "Nuevo";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_4"] = "Administración del inventario de Bitrix24";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_5"] = "Todos los documentos de uso general";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_6"] = "Administración del inventario perfectamente integrada con el CRM";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_7"] = "Catálogo de productos unificado";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_8"] = "Productos ilimitados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_9"] = "Almacenes ilimitados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_10"] = "Empleados de almacén ilimitados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_11"] = "Escáner del código de barras móvil";
