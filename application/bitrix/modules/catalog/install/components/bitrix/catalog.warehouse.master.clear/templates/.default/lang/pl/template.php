<?php
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_1"] = "Włącz zarządzanie asortymentem";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_2"] = "Wyłącz zarządzanie asortymentem";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_3"] = "Nowe";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_4"] = "Zarządzanie asortymentem Bitrix24";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_5"] = "Wszystkie powszechnie stosowane dokumenty";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_6"] = "Zarządzanie asortymentem zintegrowane z CRM";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_7"] = "Ujednolicony katalog produktów";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_8"] = "Nieograniczona liczba produktów";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_9"] = "Nieograniczona liczba magazynów";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_10"] = "Nieograniczona liczba pracowników magazynu";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_11"] = "Mobilny skaner kodów kreskowych";
