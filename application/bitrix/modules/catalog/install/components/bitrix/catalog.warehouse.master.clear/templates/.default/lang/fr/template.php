<?php
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_1"] = "Activer la gestion de l'inventaire";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_2"] = "Désactiver la gestion de l'inventaire";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_3"] = "Nouveau";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_4"] = "Gestion de l'inventaire Bitrix24";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_5"] = "Tous les documents d'usage courant";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_6"] = "Gestion de l'inventaire parfaitement intégrée au CRM";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_7"] = "Catalogue des produits unifié";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_8"] = "Produits illimités";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_9"] = "Entrepôts illimités";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_10"] = "Nombre illimité d'employés d'entrepôt";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_11"] = "Scanner de code-barres mobile";
