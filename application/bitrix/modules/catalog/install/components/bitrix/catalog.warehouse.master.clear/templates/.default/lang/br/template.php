<?php
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_1"] = "Ativar gerenciamento do inventário";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_2"] = "Desativar gerenciamento do inventário";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_3"] = "Novo";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_4"] = "Gerenciamento de inventário Bitrix24";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_5"] = "Todos os documentos amplamente utilizados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_6"] = "Gerenciamento de estoque perfeitamente integrado com CRM";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_7"] = "Catálogo de produtos unificado";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_8"] = "Produtos ilimitados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_9"] = "Depósitos ilimitados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_10"] = "Funcionários de depósito ilimitados";
$MESS["CAT_WAREHOUSE_MASTER_CLEAR_11"] = "Leitor de código de barras móvel";
