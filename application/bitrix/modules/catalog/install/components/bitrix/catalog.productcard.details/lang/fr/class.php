<?php
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "Création d'un dictionnaire portant ce nom impossible";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Ajouter des articles à la liste du dictionnaire";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Nouveau produit";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "Le produit est introuvable. Il a peut-être été supprimé.";
