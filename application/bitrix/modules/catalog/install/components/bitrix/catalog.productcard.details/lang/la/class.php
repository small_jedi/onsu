<?php
$MESS["CPD_ERROR_ADD_HIGHLOAD_BLOCK"] = "No se pudo crear un diccionario con este nombre";
$MESS["CPD_ERROR_EMPTY_DIRECTORY_ITEMS"] = "Agregar elementos a la lista del diccionario";
$MESS["CPD_NEW_PRODUCT_TITLE"] = "Nuevo Producto";
$MESS["CPD_NOT_FOUND_ERROR_TITLE"] = "No se encontró el producto. Es probable que haya sido eliminado.";
