<?php
$MESS["CANCEL_CONDUCT_BUTTON"] = "Cancelar el procesamiento";
$MESS["CONDUCT_BUTTON"] = "Procesar";
$MESS["DOCUMENT_ADD_CONTRACTOR"] = "Crear un proveedor:";
$MESS["DOCUMENT_CONTRACTOR_FIELD_PLACEHOLDER"] = "Buscar o crear un proveedor";
$MESS["DOCUMENT_CONTRACTOR_NOT_FILLED"] = "vacío";
$MESS["DOCUMENT_LINK_COPIED"] = "El enlace al objeto del inventario se copió en el portapapeles";
$MESS["DOCUMENT_PRODUCTS_ADD_PRODUCT"] = "Productos agregados";
$MESS["DOCUMENT_PRODUCTS_NOT_SHOWN"] = "#COUNT# más";
$MESS["DOCUMENT_PRODUCTS_TOTAL"] = "Total";
$MESS["SAVE_AND_CONDUCT_BUTTON"] = "Guardar y procesar";
$MESS["SLIDER_LABEL_A"] = "Recibo de existencias";
$MESS["SLIDER_LABEL_D"] = "Cancelación";
$MESS["SLIDER_LABEL_M"] = "Transferir";
$MESS["SLIDER_LABEL_S"] = "Ajuste de las existencias";
$MESS["SLIDER_LABEL_W"] = "Órdenes de venta";
