<?php
$MESS["CATALOG_STORE_SELECTOR_BEFORE_SEARCH_TITLE"] = "Trouver ou créer un entrepôt";
$MESS["CATALOG_STORE_SELECTOR_IS_EMPTY_SUBTITLE"] = "Créer un nouvel entrepôt ?";
$MESS["CATALOG_STORE_SELECTOR_IS_EMPTY_TITLE"] = "L'entrepôt est introuvable";
$MESS["CATALOG_STORE_SELECTOR_VIEW_NAME_TITLE"] = "Entrepôt";
