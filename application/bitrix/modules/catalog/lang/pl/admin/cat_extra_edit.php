<?
$MESS["CEEN_2FLIST"] = "Dodatkowa opłata";
$MESS["CEEN_ADDING"] = "Dodaj nową opłatę dodatkową";
$MESS["CEEN_APPLY"] = "Zastosuj";
$MESS["CEEN_CANCEL"] = "Anuluj";
$MESS["CEEN_DELETE_DISCOUNT"] = "Usuń dodatkową opłatę";
$MESS["CEEN_DELETE_DISCOUNT_CONFIRM"] = "Na pewno chcesz usunąć tę dodatkową opłatę?";
$MESS["CEEN_DISCOUNT_VALUE"] = "Kwota";
$MESS["CEEN_ERROR_SAVING_EXTRA"] = "Błąd podczas zapisywania dodatkowej opłaty";
$MESS["CEEN_NAME"] = "Nazwa";
$MESS["CEEN_NEW_DISCOUNT"] = "Nowa dodatkowa opłata";
$MESS["CEEN_NO_DISCOUNT"] = "Nie znaleziono dodatkowej opłaty ##ID#";
$MESS["CEEN_NO_PERMS2ADD"] = "Nie masz wystarczających uprawnień, aby dodać nową dodatkową opłatę";
$MESS["CEEN_PERCENTAGE"] = "Kwota";
$MESS["CEEN_RECALC"] = "Przelicz ceny";
$MESS["CEEN_SAVE"] = "Zapisz";
$MESS["CEEN_SAVE_ADD"] = "Dodawanie";
$MESS["CEEN_SORT"] = "Indeks sortowania";
$MESS["CEEN_TAB_DISCOUNT"] = "Dodatkowa opłata";
$MESS["CEEN_TAB_DISCOUNT_DESCR"] = "Parametry dodatkowej opłąty";
$MESS["CEEN_TIMESTAMP"] = "Data ostatniej modyfikacji:";
$MESS["CEEN_TO_LIST"] = "Dodatkowe opłaty";
$MESS["CEEN_UPDATING"] = "Edytuj parametry dodatkowej opłaty";
?>