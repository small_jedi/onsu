<?
$MESS["CEN_ADD_NEW"] = "Nowa dodatkowa opłata";
$MESS["CEN_ADD_NEW_ALT"] = "Kliknij, aby dodać nową dodatkową opłatę";
$MESS["CEN_DELETE_ALT"] = "Usuń dodatkową opłatę";
$MESS["CEN_DELETE_CONF"] = "Na pewno chcesz usunąć tę dodatkową?";
$MESS["CEN_ERROR_UPDATE"] = "Błąd aktualizacji parametrów dodatkowej opłaty";
$MESS["CEN_UPDATE_ALT"] = "Edytuj parametry dodatkowej opłaty";
$MESS["EXTRA_ACTIONS"] = "Usuń";
$MESS["EXTRA_DELETE_ERROR"] = "Błąd podczas usuwania dostosowania typu Ceny";
$MESS["EXTRA_NAME"] = "Nazwa dostosowania Ceny";
$MESS["EXTRA_NOTES"] = "Cena jest obliczana jako cena podstawowa <b>plus</b> z uwzględnienieniem procentu ceny podstawowej.<br>[Price] = [Base price] * (1 + [Percent] / 100) = [Base price] + [Base price] * [Percent] / 100";
$MESS["EXTRA_PERCENTAGE"] = "Procent";
$MESS["EXTRA_RECALCULATE"] = "Przelicz ceny";
$MESS["EXTRA_TITLE"] = "Dostosowanie Typu Ceny";
$MESS["EXTRA_UPD"] = "Zapisz";
$MESS["cat_extra_nav"] = "Dodatkowe opłaty";
?>