<?
$MESS["CEEN_2FLIST"] = "Liste des marges";
$MESS["CEEN_ADDING"] = "Ajouter une nouvelle marge";
$MESS["CEEN_APPLY"] = "Appliquer";
$MESS["CEEN_CANCEL"] = "Réinitialiser";
$MESS["CEEN_DELETE_DISCOUNT"] = "Supprimer la marge";
$MESS["CEEN_DELETE_DISCOUNT_CONFIRM"] = "Voulez-vous vraiment supprimer cette marge ?";
$MESS["CEEN_DISCOUNT_VALUE"] = "Valeur de la marge";
$MESS["CEEN_ERROR_SAVING_EXTRA"] = "Erreur d'enregistrement de la marge";
$MESS["CEEN_NAME"] = "Nom";
$MESS["CEEN_NEW_DISCOUNT"] = "Créer une nouvelle marge";
$MESS["CEEN_NO_DISCOUNT"] = "La marge ##ID# n'a pas été trouvée";
$MESS["CEEN_NO_PERMS2ADD"] = "Vous n'avez pas assez de droits pour ajouter une nouvelle marge";
$MESS["CEEN_PERCENTAGE"] = "Valeur de la marge";
$MESS["CEEN_RECALC"] = "Recalculer les prix";
$MESS["CEEN_SAVE"] = "Enregistrer";
$MESS["CEEN_SAVE_ADD"] = "Ajouter";
$MESS["CEEN_SORT"] = "Index de tri";
$MESS["CEEN_TAB_DISCOUNT"] = "Marge";
$MESS["CEEN_TAB_DISCOUNT_DESCR"] = "Paramètres de marge";
$MESS["CEEN_TIMESTAMP"] = "Date de dernière modification : ";
$MESS["CEEN_TO_LIST"] = "Liste des marges";
$MESS["CEEN_UPDATING"] = "Modification des paramètres de marge";
?>