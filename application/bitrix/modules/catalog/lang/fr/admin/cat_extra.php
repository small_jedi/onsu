<?
$MESS["CEN_ADD_NEW"] = "Nouvelle marge";
$MESS["CEN_ADD_NEW_ALT"] = "Cliquez sur pour ajouter une nouvelle supplémentaire";
$MESS["CEN_DELETE_ALT"] = "Supprimer le supplément";
$MESS["CEN_DELETE_CONF"] = "tes-vous sûr de vouloir supprimer ce supplémentaire ?";
$MESS["CEN_ERROR_UPDATE"] = "Erreur mise à jour des paramètres de charge supplémentaires";
$MESS["CEN_UPDATE_ALT"] = "Modifier les paramètres de charge supplémentaires";
$MESS["EXTRA_ACTIONS"] = "Supprimer";
$MESS["EXTRA_DELETE_ERROR"] = "Erreur lors de la suppression du type d'ajustement de prix";
$MESS["EXTRA_NAME"] = "Nom de l'ajustement de prix";
$MESS["EXTRA_NOTES"] = "Le prix est calculé comme le prix de base <b>plus</b> du [prix de base] donnée en pourcentage du prix de base.<br>[Prix] = [Base price] * (1 + [Pourcentage] / 100) = [Prix de base] + [Prix de base] * [Pourcentage] / 100";
$MESS["EXTRA_PERCENTAGE"] = "Pourcentage";
$MESS["EXTRA_RECALCULATE"] = "Recalculer les prix";
$MESS["EXTRA_TITLE"] = "Types d'ajustement de prix";
$MESS["EXTRA_UPD"] = "Enregistrer";
$MESS["cat_extra_nav"] = "Les marges";
?>