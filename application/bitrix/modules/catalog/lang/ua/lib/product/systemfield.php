<?php
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_CANNOT_DELETE_HIGHLOADBLOCK"] = "Highload-блок #NAME# є системним і не може бути видалений";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_CANNOT_UPDATE_HIGHLOADBLOCK"] = "Highload-блок #NAME# є системним і його параметри не можуть змінюватися";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_CREATE_HIGHLOADBLOCK"] = "Неможливо створити highload-блок #NAME#";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_DICTIONARY_ITEM_CREATE"] = "Помилка створення запису з кодом #CODE# в довіднику #DICTIONARY#";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_DISALLOW_UNINSTALL_HIGHLOADBLOCK"] = "Модуль highloadblock не може бути деінстальований, тому що highload-блок #NAME# є системним і використовується модулем Торговий каталог.";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_EXIST_HIGHLOADBLOCK"] = "Highload-блок #NAME# вже існує";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_HIGHLOADBLOCK_ABSENT"] = "Відсутній модуль Highload-блоки. Він необхідний для роботи з розширеними параметрами товарів. Встановіть модуль вручну на сторінці <a href=\"/bitrix/admin/module_admin.php?lang=#LANGUAGE_ID#\">списку модулів</a>";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_HIGHLOADBLOCK_CREATE_FIELD"] = "Помилка створення поля #FIELD#: #ERROR#";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_HIGHLOADBLOCK_ID_ABSENT"] = "Відсутній ідентифікатор highload-блока";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_HIGHLOAD_MODULE_ABSENT"] = "Відсутній модуль Highload-блоки.";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_INTERNAL_ERROR"] = "внутрішня помилка";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_ERR_MARKING_CODE_INTERNAL_ERROR"] = "Неможливо створити поле Група товарів: #ERROR#";
$MESS["BX_CATALOG_PRODUCT_SYSTEMFIELD_MESS_VALUE_EMPTY"] = "не обрано";
$MESS["MARKING_CODE_GROUP_FIELD_TITLE"] = "Група товарів";
$MESS["MARKING_CODE_GROUP_UF_FIELD_NAME"] = "Група товарів";
$MESS["MARKING_CODE_GROUP_UF_FIELD_XML_ID"] = "Код групи товарів";
$MESS["STORAGE_MARKING_CODE_GROUP_TITLE"] = "Типи груп товарів";
