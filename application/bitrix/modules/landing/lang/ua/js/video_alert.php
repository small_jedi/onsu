<?php
$MESS["LANDING_VIDEO_ALERT_WRONG_SOURCE"] = "Посилання на відео відсутнє";
$MESS["LANDING_VIDEO_ALERT_WRONG_SOURCE_TEXT"] = "Перевірте, чи правильно вказано посилання на відео. Допускаються тільки повні або короткі посилання на відео із сервісів Youtube, Facebook, Vimeo або Vine.";
