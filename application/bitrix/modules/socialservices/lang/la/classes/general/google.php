<?php
$MESS["socserv_google_client_id"] = "ID de cliente:";
$MESS["socserv_google_client_secret"] = "Código secreto (secreto del cliente):";
$MESS["socserv_google_form_note"] = "Utilice su cuenta de Google para iniciar sesión.";
$MESS["socserv_google_form_note_intranet"] = "Iniciar sesión con cuenta de Google";
$MESS["socserv_googleplus_note"] = "Usando parámetros de aplicaciones de Google.";
