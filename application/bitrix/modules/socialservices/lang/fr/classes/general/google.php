<?php
$MESS["socserv_google_client_id"] = "Identificateur (Client ID) : ";
$MESS["socserv_google_client_secret"] = "Code secret (Client secret) : ";
$MESS["socserv_google_form_note"] = "Utilisez votre nom d'utilisateur Google pour vous identifier sur le site.";
$MESS["socserv_google_form_note_intranet"] = "Se connecter à partir de son compte Odnoklassniki";
$MESS["socserv_googleplus_note"] = "Utilisation de paramètres de Google app.";
