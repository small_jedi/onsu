<?
if(\Bitrix\Main\Loader::includeModule('yenisite.worktime'))
{
	$APPLICATION->IncludeComponent(
	"yenisite:bitronic.worktime", 
	"bitronic2", 
	array(
		"COMPONENT_TEMPLATE" => "bitronic2",
		"LUNCH" => "Заказы, доставляются до 20:00. Время доставки уточняйте у оператора.",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000",
		"MONDAY" => "Y",
		"TUESDAY" => "Y",
		"WEDNESDAY" => "Y",
		"THURSDAY" => "Y",
		"FRIDAY" => "Y",
		"SATURDAY" => "Y",
		"SUNDAY" => "Y",
		"TIME_WORK_FROM" => "8:00",
		"TIME_WORK_TO" => "17:00",
		"TIME_WEEKEND_FROM" => "",
		"TIME_WEEKEND_TO" => "",
		"LUNCH_WEEKEND" => "",
		"HALF_DAYS" => array(
		),
		"LUNCH_HALF_DAY" => "",
		"TIME_NOT_FULL_DAY_FROM" => "",
		"TIME_NOT_FULL_DAY_TO" => "",
		"TIME_WORK" => "",
		"TIME_WEEKEND" => ""
	),
	false
);
}