<?use Bitronic2\Mobile;
$method = (mobile::isMobile()) ? 'tel' : 'callto';?>

<div class="phone-wrap">
    <a content="89237927222"
       href="<? echo $method ?>:89237927222"
       class="phone-link"
       data-tooltip title="Заказать звонок"
       data-placement="right"
    >
        <span class="flaticon-phone12"></span>
        <span itemprop="telephone">8 923 792 7222</span><br>
        <span style="font-size: small">г. Бийск</span>
    </a>
</div>
