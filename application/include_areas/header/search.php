<?
global $rz_b2_options;
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"bitronic2", 
	array(
		"NUM_CATEGORIES" => "2",
		"TOP_COUNT" => "5",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => "/catalog/#ELEMENT_CODE#.html",
		"CATEGORY_0_TITLE" => "Товары",
		"CATEGORY_0" => array(
			0 => "iblock_catalog",
		),
		"CATEGORY_0_iblock_catalog" => array(
			0 => "46",
		),
		"CATEGORY_1_TITLE" => "Новости",
		"CATEGORY_1" => array(
			0 => "iblock_news",
		),
		"CATEGORY_1_iblock_catalog" => array(
			0 => "all",
		),
		"CATEGORY_OTHERS_TITLE" => "Другое",
		"SHOW_INPUT" => "Y",
		"CONTAINER_ID" => "search",
		"INPUT_ID" => "search-field",
		"PRICE_CODE" => array(
			0 => "розничная интернет-магаз.",
		),
		"SHOW_PREVIEW" => "Y",
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75",
		"CONVERT_CURRENCY" => "N",
		"ORDER" => "date",
		"USE_LANGUAGE_GUESS" => "N",
		"RESIZER_SEARCH_TITLE" => "10",
		"PRICE_VAT_INCLUDE" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"CURRENCY_ID" => "RUB",
		"COMPONENT_TEMPLATE" => "bitronic2",
		"EXAMPLE_ENABLE" => "Y",
		"EXAMPLES" => array(
			0 => "Пельмени",
			1 => "Вареники",
			2 => "",
		),
		"SHOW_CATEGORY_SWITCH" => ($rz_b2_options["block_search_category"]!=="N"?"Y":"N"),
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CATEGORY_1_iblock_news" => array(
			0 => "all",
		)
	),
	false
);