<title>Преимущества</title><div class="row benefits hidden-xs wow fadeIn">
	<div class="benefit">
		<div class="img-wrap">
			<span data-picture data-alt="Доставляем!">
				<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/delivery.png"></span>
				<span data-src="" data-media="(max-width: 767px)"></span>

				<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
				<noscript>
					<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/delivery.png" alt="Доставляем!">
				</noscript>
			</span>
		</div>
		<div class="content">
			<header>Удобная доставка</header>
			<p>Производим доставку в удобное для Вас время днем и вечером.</p>
		</div>
	</div>
    <div class="benefit">
		<div class="img-wrap">
			<span data-picture data-alt="Гарантируем!">
				<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/OK.png"></span>
				<span data-src="" data-media="(max-width: 767px)"></span>

				<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
				<noscript>
					<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/OK.png" alt="Гарантируем!">
				</noscript>
			</span>
		</div>
		<div class="content">
			<header>Гарантия качества</header>
			<p>Если Вас не устроило качество продукта, мы заменим товар или вернем Вам деньги.</p>
		</div>
	</div>
    <div class="benefit">
		<div class="img-wrap">
			<span data-picture data-alt="Дружим!">
				<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/agree.png"></span>
				<span data-src="" data-media="(max-width: 767px)"></span>

				<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
				<noscript>
					<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/agree.png" alt="Дружим!">
				</noscript>
			</span>
		</div>
		<div class="content">
			<header>Свежие продукты от производителя</header>
			<p>Доставляем самые свежие и вкусные продукты напрямую от Алтайских производителей</p>
		</div>
	</div>
    <div class="benefit">
		<div class="img-wrap">
			<span data-picture data-alt="Выгодно!">
				<span data-src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/sale.png"></span>
				<span data-src="" data-media="(max-width: 767px)"></span>

				<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
				<noscript>
					<img src="<?=SITE_TEMPLATE_PATH?>/pictures/benefits/sale.png" alt="Выгодно!">
				</noscript>
			</span>	
		</div>
		<div class="content">
			<header>Экономия времени и комфорт</header>
			<p>Экономьте свое время, заказывайте и получайте готовые продукты в комфортных для Вас условиях.</p>
		</div>
	</div>
</div>