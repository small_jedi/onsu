<?
if(Bitrix\Main\Loader::includeModule('yenisite.oneclick')
&& Bitrix\Main\Loader::includeModule('sale'))
{
    global $rz_b2_options;
	$APPLICATION->IncludeComponent(
	"yenisite:oneclick.buy", 
	"bitronic2", 
	array(
		"COMPONENT_TEMPLATE" => "bitronic2",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "#CATALOG_IBLOCK_ID#",
		"IBLOCK_ELEMENT_ID" => $_REQUEST["id"],
		"PERSON_TYPE_ID" => "1",
		"SHOW_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
			2 => "PHONE",
		),
		"REQ_FIELDS" => array(
			0 => "PHONE",
		),
		"ALLOW_AUTO_REGISTER" => "Y",
		"USE_CAPTCHA" => "N",
		"USE_CAPTCHA_FORCE" => $rz_b2_options["captcha-quick-buy"],
		"MESSAGE_OK" => "Ваш заказ принят, его номер - #ID#. Менеджер свяжется с вами в ближайшее время. Спасибо что выбрали нас!",
		"PAY_SYSTEM_ID" => "1",
		"DELIVERY_ID" => "2",
		"AS_EMAIL" => "EMAIL",
		"AS_NAME" => "FIO",
		"FIELD_CLASS" => "textinput",
		"FIELD_PLACEHOLDER" => "Y",
		"FIELD_QUANTITY" => "Y",
		"SEND_REGISTER_EMAIL" => "Y",
		"EMPTY" => $arParams["EMPTY"],
		"USER_REGISTER_EVENT_NAME" => "USER_INFO",
		"OFFER_PROPS" => $arProps,
		"BUY_IN_BASKET" => "N",
		"COMMENTS" => "Заказ в 1 клик"
	),
	false
);
}?>