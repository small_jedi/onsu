<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="title-h3">Способы оплаты</div>
<ul>
    <li>банковская карта (Visa/Mastercard/Maestro),</li>
    <li>наличный расчет</li>
</ul>

<div class="title-h3">Способы доставки</div>
<p>Заказы, принятые <b>до 17:00</b> могут быть доставлены <b>в этот же день.</b></p>
<p>Заказы, принятые <b>после 17:00</b> доставляются <b>на следующий день.</b></p>
<p><a href="/dostavka-oplata/" target="_blank" class="link"><span class="text">Подробнее об условиях доставки &gt;&gt;</span></a></p>

<div class="title-h3">Возврат товаров</div>
<p>Если Вы по какой-либо причине не удовлетворены качеством, вкусом или внешним видом продукта, мы вернем Вам деньги
    или заменим продукт!</p>