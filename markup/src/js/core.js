import $ from 'jquery';
import 'bootstrap';
import React from 'react';

window.$ = window.jQuery = $;
import './include/sliders';
import './include/_jquery-code';

import './react/AddToBasketButton';
import './react/AddWishlistButton';
import './react/City/Dialog';