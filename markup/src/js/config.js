let url = location.protocol + '//' + location.hostname + '/';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") url = 'http://localhost:27016/';
// if (location.hostname === "localhost" || location.hostname === "127.0.0.1") url = 'http://local.provianta.ru/';

const config = {
    // restElement: url + 'rest/element.php',
    restBasket: url + 'rest/basket/',
    restFavorite: url + 'rest/favorite/',
    // restEquipment: url + 'rest/equipment.php',
    // restEquipmentItems: url + 'rest/equipment_items.php',
};


export default config;