import Swiper from "swiper";

const home_slider = new Swiper(".home-slider", {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});
const about_slider = new Swiper(".about-slider", {
    slidesPerView: 3,
    autoplay: true,
    spaceBetween: 10,
    loop: true,
});

const product_info_offers_slider = new Swiper(".product-info-offers-slider", {
    slidesPerView: 1,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        450: {
            slidesPerView: 2,
        },
        920: {
            slidesPerView: 4,
        },
    }
});

const profile_sales = new Swiper(".profile-sales", {
    autoplay: true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

document.addEventListener('DOMContentLoaded', () => {
    // window.onload = () => {
    const swiper = new Swiper(".mySwiper", {
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesProgress: true,
    });
    const swiper2 = new Swiper(".mySwiper2", {
        spaceBetween: 10,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: swiper,
        },
    });
    // }
});
