import React, {useState} from "react";
import RestRequest from "../../tool/RestRequest";
import config from "../../config";

export default function AddToBasketButton({product_id, isExist = false}) {

    const [inBasket, setInBasket] = useState(isExist);

    let add2Basket = (event) => {
        event.preventDefault();

        let params = new FormData();
        params.append('product_id', product_id);

        RestRequest.post(config.restBasket, {
            body: params
        }).then(data => {

            if (data.status === 200) {
                const counter = document.querySelector('.js-basket-counter');

                if (counter) {

                    const count = parseInt(counter.textContent ?? 0);

                    if (count <= 0 && !counter.classList.contains('is-active')) {
                        counter.classList.add('is-active')
                    }

                    counter.textContent = count + 1;
                }


                setInBasket(!inBasket);
            }
        });

    };

    return <button onClick={add2Basket.bind(this)} className="btn-main">{inBasket ? 'В корзине' : 'В корзину'}</button>
}