import ReactDom from "react-dom";
import React from "react";
import AddToBasketButton from "./AddToBasketButton";

let buttonsAdd2Basket = document.querySelectorAll('.add-to-basket-react');
if (buttonsAdd2Basket) {
    buttonsAdd2Basket.forEach((el) => {
        let product_id = el.getAttribute('data-product-id');
        let is_exist = el.getAttribute('data-is-exist') === 'true';
        if (product_id) ReactDom.render(<AddToBasketButton isExist={is_exist} product_id={product_id}/>, el);
    });
}