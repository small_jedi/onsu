import ReactDom from "react-dom";
import React from "react";
import AddWishlistInList from "./AddWishlistInList";

let buttonsAdd2WishlistList = document.querySelectorAll('.add-to-wishlist-list-react');
if (buttonsAdd2WishlistList) {
    buttonsAdd2WishlistList.forEach((el) => {
        let product_id = el.getAttribute('data-product-id');
        let is_exist = el.getAttribute('data-is-exist') === 'true';
        if (product_id) ReactDom.render(<AddWishlistInList isExist={is_exist} product_id={product_id}/>, el);
    });
}