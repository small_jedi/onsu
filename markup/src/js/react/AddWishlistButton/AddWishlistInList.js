import React, {useState} from "react";
import config from "../../config";
import RestRequest from "../../tool/RestRequest";

export default function AddWishlistInList({product_id, isExist = false}) {

    const [isEx, setIsEx] = useState(isExist);

    function add2wish(product_id) {

        let params = new FormData();
        params.append('product_id', product_id);

        RestRequest.post(config.restFavorite, {
            body: params
        }).then(data => {

            if (data.status === 200) {
                const counter = document.querySelector('.js-favorite-counter');

                if (counter) {

                    const count = parseInt(counter.textContent ?? 0);

                    if (count <= 0 && !counter.classList.contains('is-active')) {
                        counter.classList.add('is-active')
                    }

                    counter.textContent = count + 1;
                }
            }

            console.log(data);
            setIsEx(!isEx);
        });
    }


    function handleClick(event) {
        add2wish(product_id)
    }

    return <div onClick={handleClick.bind(this)} className={`${isEx ? 'in-favorite' : ''}`}>
        <i className="fas fa-heart"/>
    </div>;
}