import React, {useEffect, useState} from "react";
import Fancybox from "./Fancybox";
import FindCity from "../Form/FindCity";
import CityList from "./CityList";


const CityDialog = ({settgins}) => {

    const [models, setModels] = useState(settgins.models);

    return <>
        <Fancybox>
            <div className="header-select-city">
                <div className="header-select-city-title">Ваш город:</div>
                <div data-fancybox="dialog" data-src="#dialog-content" className="header-select-city-target">Барнаул</div>
            </div>

            <div id="dialog-content" className="header-select-city-dialog">

                <h2>Выбрать ваш город</h2>

                <FindCity actions={{setModels}} models={models}/>

                <CityList models={settgins.previewModels}/>
            </div>
        </Fancybox>
    </>;
};

export default CityDialog;