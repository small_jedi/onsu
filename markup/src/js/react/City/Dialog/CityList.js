import React from "react";

const CityList = ({models}) => {

    return <div className="header-select-city-list">
        {models.map((city, index) => {
            return <div key={index} className="header-select-city-list-item site-link">{city.name}</div>
        })}
    </div>
};
export default CityList;