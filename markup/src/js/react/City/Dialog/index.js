import ReactDom from "react-dom";
import React from "react";
import CityDialog from "./CityDialog";

let items = document.querySelectorAll('.city-dialog-react');
if (items) {
    items.forEach(element => {

        const settings = {};
        settings.models = getDataFromAttribute(element, 'data-models');
        settings.previewModels = getDataFromAttribute(element, 'data-preview-models');

        ReactDom.render(<CityDialog settgins={settings}/>, element);
    });
}

function getDataFromAttribute(element, attribute) {

    if (!element.getAttribute(attribute)) return false;

    return JSON.parse(element.getAttribute(attribute));

}